#!/bin/sh

TIMEOUT="5s";
LOG_FOLDER="./log"
LOG_OUTPUT="$LOG_FOLDER/output.log";
LOG_ERROR="$LOG_FOLDER/error.log";
STARTED_PATTERN="\[STARTED\]"
ERROR_PATTERN="\[ERROR\]"

printLogAndExit(){
    echo "------------------------- $LOG_OUTPUT -------------------------";
    cat "$LOG_OUTPUT";

    echo "------------------------- $LOG_ERROR -------------------------";
    cat "$LOG_ERROR";

    exit 1;
}

echo "Starting application test...";

mkdir $LOG_FOLDER;
echo "Created folder '$LOG_FOLDER' for logs...";

java -jar ./target/*.jar 2> $LOG_ERROR > $LOG_OUTPUT &
echo "Started JAR application..."
echo "Redirected standard output to $LOG_OUTPUT and error to $LOG_ERROR ...";

sleep $TIMEOUT;
echo "Wake up after a $TIMEOUT timeout...";

count_errors=$(grep -c "$ERROR_PATTERN" "$LOG_OUTPUT");
echo "Found $count_errors errors when starting the application...";
if [ "$count_errors" -ne 0 ]
  then
    echo "Failed to load the application...";
    printLogAndExit;
fi

is_start_completed=$(grep -c "$STARTED_PATTERN" "$LOG_OUTPUT");

if [ "$is_start_completed" -eq 1 ]
  then
    echo "The application started successfully!";
    exit 0;
fi

echo "The application seems not already started. Please try again...";
printLogAndExit;