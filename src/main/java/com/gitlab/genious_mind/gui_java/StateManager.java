package com.gitlab.genious_mind.gui_java;

import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.util.PropertiesService;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.name.Names;
import com.google.inject.util.Modules;
import lombok.extern.log4j.Log4j2;

import java.util.Properties;

@Log4j2
public class StateManager implements Context {

    private final PropertiesService propertiesService;
    private final Module implModule;
    private final Properties properties;
    private Injector injector;
    private State currentState;

    public StateManager(Module implModule, PropertiesService propertiesService) {
        this.implModule = implModule;
        this.propertiesService = propertiesService;

        this.properties = loadAndSaveProperties(propertiesService);
        this.injector = Guice.createInjector(Modules.combine(implModule, getPropertiesModule(properties)));
    }

    @Override
    public void changeState(StateEnum state, Object... args) {
        if (null != this.currentState) {
            this.currentState.stop();
        }
        this.currentState = this.injector.getInstance(state.getClassType());
        this.currentState.start(this, args);
    }

    @Override
    public void updateUserProperties(Properties props) {
        propertiesService.updateUserProperties(props);
        properties.putAll(props);
        this.injector = Guice.createInjector(Modules.combine(implModule, getPropertiesModule(properties)));
    }

    public static <T> T getObjectFromArgs(Object[] args, int position, Class<T> classType) {
        if (null == args || position >= args.length || !classType.isInstance(args[position])) {
            throw new ClassCastException();
        }

        return classType.cast(args[position]);
    }

    private static Module getPropertiesModule(Properties properties) {
        return binder -> Names.bindProperties(binder, properties);
    }

    private static Properties loadAndSaveProperties(PropertiesService propertiesService) {
        Properties props = new Properties();
        boolean doesUserPropertiesFileExist = true;

        props.putAll(propertiesService.loadDefaultProperties());

        try {
            props.putAll(propertiesService.loadUserProperties());
        } catch (Exception e) {
            doesUserPropertiesFileExist = false;
            log.warn(e);
        }

        if (!doesUserPropertiesFileExist) {
            propertiesService.saveUserProperties(props);
            log.warn("User properties file overwritten!");
        }

        return props;
    }

}
