package com.gitlab.genious_mind.gui_java.common;

import com.gitlab.genious_mind.gui_java.util.JsonParser;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class Packet {

  private static final JsonParser<Packet> jsonParser = new JsonParser<>(new TypeReference<Packet>() {
  });

  private final String address;
  private final int port;
  private final String message;

  @JsonCreator
  public Packet(@JsonProperty("address") String address, @JsonProperty("port") int port, @JsonProperty("message") String message) {
    this.address = address;
    this.port = port;
    this.message = message;
  }

  public Packet(String address, int port) {
    this(address, port, "");
  }

  public static Packet fromJson(String message) {
    return jsonParser.fromJson(message);
  }

  public static String toJson(Packet packet) {
    return jsonParser.toJson(packet);
  }

  public boolean isSameSender(Packet packet) {
    return getPort() == packet.getPort() && getAddress().equals(packet.getAddress());
  }

}
