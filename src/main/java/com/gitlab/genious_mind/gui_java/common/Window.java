package com.gitlab.genious_mind.gui_java.common;

import java.io.Closeable;

public interface Window extends AutoCloseable, Closeable {

    void showServerDisconnectionError();

    void showInfo(String title, String message);

    void showWarning(String title, String message);

    void showError(String title, String message);

    String showPrompt(String title, String message);

    boolean showConfirm(String title, String message);

    void close();

}
