package com.gitlab.genious_mind.gui_java.common.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.gitlab.genious_mind.gui_java.util.JsonParser;
import lombok.Getter;

@Getter
public class Message<T> {

    private final MessageType type;
    private final T content;

    public Message(MessageType type) {
        this(type, (T) null);
    }

    @JsonCreator
    public Message(@JsonProperty("type") MessageType type, @JsonProperty("content") T content) {
        if (null == type) {
            throw new NullPointerException("type must be not null!");
        }
        this.type = type;
        this.content = content;
    }

    public String toJson() {
        return new JsonParser<>(new TypeReference<>() {
        }).toJson(this);
    }

    public static MessageType getTypeFromJson(String json) {
        return new JsonParser<>(new TypeReference<Message<Object>>() {
        }).fromJson(json).getType();
    }

    public static <T> T getMessageFromJson(TypeReference<Message<T>> type, String json) {
        return new JsonParser<>(type).fromJson(json).getContent();
    }

}
