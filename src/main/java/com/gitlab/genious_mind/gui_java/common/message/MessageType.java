package com.gitlab.genious_mind.gui_java.common.message;

public enum MessageType {

    UPDATE_SERVER_LIST,
    START,
    ATTEMPT_REQUEST,
    ATTEMPT_RESPONSE,
    WINNER,
    LOSER,
    DISCONNECTED,
    GAME_ALREADY_STARTED,
    MAX_PLAYER_REACHED,
    PLAYER_DISCONNECTED,
    TIME,

}
