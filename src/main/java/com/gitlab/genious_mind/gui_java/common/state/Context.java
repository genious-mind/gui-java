package com.gitlab.genious_mind.gui_java.common.state;

import java.util.Properties;

public interface Context {

    void changeState(StateEnum state, Object... args);

    void updateUserProperties(Properties props);

}
