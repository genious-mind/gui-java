package com.gitlab.genious_mind.gui_java.common.state;

public interface State {

    void start(Context context, Object... args);

    void stop();

}
