package com.gitlab.genious_mind.gui_java.common.state;

import com.gitlab.genious_mind.gui_java.states._continue.Continue;
import com.gitlab.genious_mind.gui_java.states._new.New;
import com.gitlab.genious_mind.gui_java.states.configuration.Configuration;
import com.gitlab.genious_mind.gui_java.states.direct.DirectConnection;
import com.gitlab.genious_mind.gui_java.states.discoverer.ServerDiscoverer;
import com.gitlab.genious_mind.gui_java.states.game.client.ClientGame;
import com.gitlab.genious_mind.gui_java.states.language.LanguageSelection;
import com.gitlab.genious_mind.gui_java.states.lobby.client.ClientLobby;
import com.gitlab.genious_mind.gui_java.states.lobby.server.ServerLobby;
import com.gitlab.genious_mind.gui_java.states.menu.main.MainMenu;
import com.gitlab.genious_mind.gui_java.states.menu.multiplayer.MultiPlayerMenu;
import com.gitlab.genious_mind.gui_java.states.menu.singleplayer.SinglePlayerMenu;
import com.gitlab.genious_mind.gui_java.states.record.Record;
import com.gitlab.genious_mind.gui_java.states.single.SinglePlayer;
import lombok.Getter;

@Getter
public enum StateEnum {

    MAIN_MENU(MainMenu.class),
    LANGUAGE(LanguageSelection.class),
    SINGLE_PLAYER_MENU(SinglePlayerMenu.class),
    CONTINUE(Continue.class),
    NEW(New.class),
    CONFIGURATION(Configuration.class),
    RECORD(Record.class),
    SINGLE_PLAYER(SinglePlayer.class),
    MULTI_PLAYER_MENU(MultiPlayerMenu.class),
    SERVER_DISCOVERER(ServerDiscoverer.class),
    DIRECT_CONNECTION(DirectConnection.class),
    CLIENT_LOBBY(ClientLobby.class),
    SERVER_LOBBY(ServerLobby.class),
    CLIENT_GAME(ClientGame.class);

    private final Class<? extends State> classType;

    StateEnum(Class<? extends State> classType) {
        this.classType = classType;
    }

}
