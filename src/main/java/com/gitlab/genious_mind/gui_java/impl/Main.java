package com.gitlab.genious_mind.gui_java.impl;

import com.gitlab.genious_mind.gui_java.StateManager;
import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.impl.filesystem.JavaPropertiesService;
import com.gitlab.genious_mind.gui_java.impl.filesystem.PropertiesLoader;
import com.gitlab.genious_mind.gui_java.impl.utils.ClassModule;
import com.gitlab.genious_mind.gui_java.impl.db.module.DbModule;
import com.gitlab.genious_mind.gui_java.util.PropertiesService;
import com.google.inject.Module;
import com.google.inject.util.Modules;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.net.URISyntaxException;

@Log4j2
public class Main {

    private static final String CONTINUE = "continue";
    private static final String SINGLE_PLAYER = "singleplayer";
    private static final String CLIENT_DIRECT = "direct";
    private static final String CLIENT_DISCOVERER = "discoverer";
    private static final String SERVER = "server";

    public static void main(String[] args) throws IOException, URISyntaxException {
        try {
            Module module = Modules.combine(new ClassModule(), new DbModule());
            long time = System.currentTimeMillis();
            Context context;

            try (PropertiesLoader propertiesLoader = new PropertiesLoader()) {
                PropertiesService propertiesService = new JavaPropertiesService(propertiesLoader);
                context = new StateManager(module, propertiesService);
            }

            context.changeState(getStateFromArgs(args));

            log.info("[STARTED] Application successfully started in {}s!", (System.currentTimeMillis() - time) / 1000.0);
        } catch (Exception e) {
            log.error("Failed to load the application!", e);
        }
    }

    private static StateEnum getStateFromArgs(String[] args) {
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case SERVER:
                    return StateEnum.SERVER_LOBBY;
                case CLIENT_DIRECT:
                    return StateEnum.DIRECT_CONNECTION;
                case CLIENT_DISCOVERER:
                    return StateEnum.SERVER_DISCOVERER;
                case CONTINUE:
                    return StateEnum.CONTINUE;
                case SINGLE_PLAYER:
                    return StateEnum.NEW;
            }
        }
        return StateEnum.MAIN_MENU;
    }

}
