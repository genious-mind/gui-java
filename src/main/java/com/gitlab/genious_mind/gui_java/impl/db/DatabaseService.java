package com.gitlab.genious_mind.gui_java.impl.db;

import com.gitlab.genious_mind.gui_java.impl.db.dao.MatchDao;
import com.gitlab.genious_mind.gui_java.impl.db.entity.Attempt;
import com.gitlab.genious_mind.gui_java.impl.db.entity.Match;
import com.gitlab.genious_mind.gui_java.util.MatchService;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;
import com.gitlab.genious_mind.gui_java.util.core.MastermindAttempt;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
public class DatabaseService implements MatchService {

    private final MatchDao matchDao;

    @Inject
    public DatabaseService(MatchDao matchDao) {
        this.matchDao = matchDao;
    }

    @Override
    public Mastermind loadMastermind() {
        Match match = matchDao.findOpenMatch().orElseThrow(EntityNotFoundException::new);

        return getMastermind(match);
    }

    @Override
    public void closeOpenMatch() {
        List<Match> matches = matchDao.findOpenMatches();
        matches.forEach(match -> match.setStatus(Match.Status.ABANDONED));
        matchDao.update(matches);
    }

    @Override
    public void insertNewMatch(Mastermind mastermind) {
        Match match = new Match();

        copyInMatch(mastermind, match);

        matchDao.insert(match);
    }

    private void copyInMatch(Mastermind mastermind, Match match) {
        match.setNumberToGuess(mastermind.getNumberToGuess());
        match.setMaxDigit(mastermind.getConfig().getMaxDigit());
        match.setNumberLength(mastermind.getConfig().getNumberLength());
        match.setElapsedSeconds(mastermind.getElapsedSeconds());
        match.setStartDate(new Timestamp(mastermind.getStartTime()));
        match.setStatus(mastermind.isGameOver() ? Match.Status.WON : Match.Status.OPEN);
        match.setScore(mastermind.getScore());
        match.getAttempts().clear();
        match.getAttempts().addAll(getAttemptList(mastermind.getAttempts()));
    }

    @Override
    public void saveMatch(Mastermind mastermind) {
        Match match = matchDao.findOpenMatch().orElseThrow(EntityNotFoundException::new);

        copyInMatch(mastermind, match);

        matchDao.update(match);
    }

    @Override
    public boolean isThereAnOpenMatch() {
        return matchDao.findOpenMatch().isPresent();
    }

    @Override
    public Collection<Mastermind> findAllWonMatches() {
        return matchDao.findWonMatches().stream()
                .map(DatabaseService::getMastermind)
                .sorted(Comparator.comparing(Mastermind::getScore))
                .collect(Collectors.toList());
    }

    private static Mastermind getMastermind(Match match) {
        return new Mastermind(
                new MastermindConfig(match.getMaxDigit(), match.getNumberLength()),
                match.getNumberToGuess(),
                match.getStartDate().getTime(),
                match.getElapsedSeconds(),
                getMastermindAttemptList(match.getAttempts()),
                match.getScore());
    }

    private static List<Attempt> getAttemptList(Collection<MastermindAttempt> attempts) {
        return attempts.stream().map(a -> {
            Attempt attempt = new Attempt();
            attempt.setNumber(a.getAttempt());
            attempt.setDigitsExact(a.getDigitsExact());
            attempt.setDigitsPresent(a.getDigitsPresent());
            attempt.setElapsedSeconds(a.getElapsedSeconds());
            return attempt;
        }).collect(Collectors.toList());
    }

    private static List<MastermindAttempt> getMastermindAttemptList(Collection<Attempt> attempts) {
        return attempts.stream().map(attempt -> new MastermindAttempt(
                attempt.getNumber(),
                attempt.getDigitsExact(),
                attempt.getDigitsPresent(),
                attempt.getElapsedSeconds()
        )).collect(Collectors.toList());
    }

}
