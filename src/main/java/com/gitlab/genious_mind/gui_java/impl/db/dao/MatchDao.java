package com.gitlab.genious_mind.gui_java.impl.db.dao;

import com.gitlab.genious_mind.gui_java.impl.db.entity.Match;
import com.j256.ormlite.dao.Dao;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class MatchDao {

    private final Dao<Match, Long> matchDao;

    @Inject
    public MatchDao(Dao<Match, Long> matchDao) {
        this.matchDao = matchDao;
    }

    public void insert(Match match) {
        try {
            matchDao.create(match);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void update(Match match) {
        try {
            matchDao.update(match);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void update(List<Match> matches) {
        try {
            for (Match match : matches) {
                matchDao.update(match);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Match> findWonMatches() {
        return findMatches(Match.Status.WON);
    }

    public List<Match> findOpenMatches() {
        return findMatches(Match.Status.OPEN);
    }

    public List<Match> findMatches(Match.Status status) {
        try {
            return matchDao.queryForEq(Match.STATUS, status);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Optional<Match> findOpenMatch() {
        List<Match> matches = findOpenMatches();
        switch (matches.size()) {
            case 0:
                return Optional.empty();
            case 1:
                return Optional.of(matches.get(0));
            default:
                throw new RuntimeException();
        }
    }

}
