package com.gitlab.genious_mind.gui_java.impl.db.entity;

import com.google.common.primitives.Ints;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Converter
public class ArrayConverter implements AttributeConverter<int[], String> {

    private static final String DELIMITER = " - ";

    public static String convertToString(int[] numberToGuess) {
        return Arrays.stream(numberToGuess).mapToObj(Integer::toString).collect(Collectors.joining(DELIMITER));
    }

    public static int[] convertToIntArray(String string) {
        String[] array = string.split(DELIMITER, -1);
        List<Integer> arrayInteger = Arrays.stream(array).map(Integer::parseInt).collect(Collectors.toList());
        return Ints.toArray(arrayInteger);
    }

    @Override
    public String convertToDatabaseColumn(int[] array) {
        return convertToString(array);
    }

    @Override
    public int[] convertToEntityAttribute(String string) {
        return convertToIntArray(string);
    }

}
