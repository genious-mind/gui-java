package com.gitlab.genious_mind.gui_java.impl.db.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = Attempt.TABLE_NAME)
@SequenceGenerator(name = Attempt.SEQUENCE_NAME, initialValue = 1, allocationSize = 1)
public class Attempt {

    protected static final String TABLE_NAME = "ATTEMPT";
    protected static final String SEQUENCE_NAME = "ATTEMPT_SEQUENCE";
    public static final String ID = "ID";
    public static final String MATCH_ID = "MATCH_ID";
    public static final String NUMBER = "NUMBER";
    public static final String EXACT_DIGITS = "EXACT_DIGITS";
    public static final String PRESENT_DIGITS = "PRESENT_DIGITS";
    public static final String ELAPSED_SECONDS = "ELAPSED_SECONDS";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    @Column(nullable = false, name = ID)
    private long id;

    @OneToOne
    @Column(name = MATCH_ID, nullable = false)
    private Match match;

    @Column(nullable = false, name = NUMBER)
    private String number;

    @Column(nullable = false, name = EXACT_DIGITS)
    private int digitsExact;

    @Column(nullable = false, name = PRESENT_DIGITS)
    private int digitsPresent;

    @Column(nullable = false, name = ELAPSED_SECONDS)
    private long elapsedSeconds;

    public void setNumber(int[] numberToGuess) {
        this.number = ArrayConverter.convertToString(numberToGuess);
    }

    public int[] getNumber() {
        return ArrayConverter.convertToIntArray(number);
    }

}
