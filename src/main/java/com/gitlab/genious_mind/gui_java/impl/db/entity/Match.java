package com.gitlab.genious_mind.gui_java.impl.db.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;

@Data
@Entity(name = Match.TABLE_NAME)
@SequenceGenerator(name = Match.SEQUENCE_NAME, initialValue = 1, allocationSize = 1)
public class Match {

    public enum Status {
        OPEN, ABANDONED, WON,
    }

    protected static final String TABLE_NAME = "MATCH";
    protected static final String SEQUENCE_NAME = "MATCH_SEQUENCE";
    public static final String ID = "ID";
    public static final String NUMBER_TO_GUESS = "NUMBER_TO_GUESS";
    public static final String MAX_DIGIT = "MAX_DIGIT";
    public static final String NUMBER_LENGTH = "NUMBER_LENGTH";
    public static final String ELAPSED_SECONDS = "ELAPSED_SECONDS";
    public static final String START_DATE = "START_DATE";
    public static final String STATUS = "STATUS";
    public static final String SCORE = "SCORE";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    @Column(nullable = false, name = ID)
    private long id;

    @Column(nullable = false, name = NUMBER_TO_GUESS)
    private String numberToGuess;

    @Column(nullable = false, name = MAX_DIGIT)
    private int maxDigit;

    @Column(nullable = false, name = NUMBER_LENGTH)
    private int numberLength;

    @Column(nullable = false, name = ELAPSED_SECONDS)
    private long elapsedSeconds;

    @Column(nullable = false, name = START_DATE)
    private Timestamp startDate;

    @Column(nullable = false, name = STATUS)
    private Status status;

    @Column(name = SCORE)
    private Long score;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = Match.class)
    private Collection<Attempt> attempts = Collections.emptyList();

    public void setNumberToGuess(int[] numberToGuess) {
        this.numberToGuess = ArrayConverter.convertToString(numberToGuess);
    }

    public int[] getNumberToGuess() {
        return ArrayConverter.convertToIntArray(numberToGuess);
    }

}
