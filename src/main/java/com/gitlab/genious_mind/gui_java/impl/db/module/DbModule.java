package com.gitlab.genious_mind.gui_java.impl.db.module;

import com.gitlab.genious_mind.gui_java.impl.db.entity.Attempt;
import com.gitlab.genious_mind.gui_java.impl.db.entity.Match;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import lombok.extern.log4j.Log4j2;

import javax.inject.Named;
import javax.inject.Singleton;
import java.sql.SQLException;

@Log4j2
public class DbModule extends AbstractModule {

    private enum DDLOptionType {
        NONE, UPDATE, CREATE,
    }

    @Singleton
    @Provides
    protected ConnectionSource provideDatabaseConnection(
            @Named("database.connection.url") String url,
            DDLOptionType ddlOptionType) throws SQLException {
        ConnectionSource connectionSource = new JdbcConnectionSource(url);
        executeDDL(connectionSource, ddlOptionType, Match.class);
        executeDDL(connectionSource, ddlOptionType, Attempt.class);
        return connectionSource;
    }

    @Singleton
    @Provides
    protected Dao<Match, Long> provideMatchDao(ConnectionSource connectionSource) throws SQLException {
        return DaoManager.createDao(connectionSource, Match.class);
    }

    @Singleton
    @Provides
    protected DDLOptionType provideDDLOption(@Named("database.ddl.option") String ddlOption) {
        try {
            return DDLOptionType.valueOf(ddlOption.toUpperCase());
        } catch (Exception e) {
            log.warn("Cannot convert '{}' to DDL option!", ddlOption);
            return DDLOptionType.NONE;
        }
    }

    private static void executeDDL(ConnectionSource connectionSource, DDLOptionType DDLOptionType, Class<?> classType) throws SQLException {
        switch (DDLOptionType) {
            case NONE:
                TableUtils.createTableIfNotExists(connectionSource, classType);
                break;
            case CREATE:
                TableUtils.dropTable(connectionSource, classType, false);
                TableUtils.createTable(connectionSource, classType);
                break;
            case UPDATE:
                throw new UnsupportedOperationException();
        }
    }

}
