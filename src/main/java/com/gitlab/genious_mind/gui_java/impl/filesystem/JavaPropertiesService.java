package com.gitlab.genious_mind.gui_java.impl.filesystem;

import com.gitlab.genious_mind.gui_java.util.PropertiesService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class JavaPropertiesService implements PropertiesService {

    private static final String CONFIG_PROPERTIES_FILE = "/config.properties";
    private static final String DEFAULT_PROPERTIES_FILE = "/default.properties";
    private static final String USER_PROPERTIES_FILE = "./user.properties";
    private final PropertiesLoader propertiesLoader;

    @Inject
    public JavaPropertiesService(PropertiesLoader propertiesLoader) {
        this.propertiesLoader = propertiesLoader;
    }

    @Override
    public Properties loadDefaultProperties() {
        try {
            Properties p = new Properties();
            p.putAll(loadProperties(CONFIG_PROPERTIES_FILE));
            p.putAll(loadProperties(DEFAULT_PROPERTIES_FILE));
            return p;
        } catch (URISyntaxException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Properties loadProperties(String filePath) throws URISyntaxException, IOException {
        Path path = propertiesLoader.getResourceFromClasspath(filePath);
        return propertiesLoader.getPropertiesFromFile(path);
    }

    @Override
    public Properties loadUserProperties() {
        try {
            Path path = propertiesLoader.getResourceFromExternal(USER_PROPERTIES_FILE);
            return propertiesLoader.getPropertiesFromFile(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void saveUserProperties(Properties properties) {
        try {
            Path path = propertiesLoader.getResourceFromExternal(USER_PROPERTIES_FILE);
            propertiesLoader.savePropertiesToFile(path, filterUserProperties(properties));
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private Properties filterUserProperties(Properties properties) throws IOException, URISyntaxException {
        Set<Object> defaultKeys = loadProperties(DEFAULT_PROPERTIES_FILE).keySet();

        Map<Object, Object> map = properties.entrySet().stream()
                .filter(entry -> defaultKeys.contains(entry.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Properties props = new Properties();
        props.putAll(map);
        return props;
    }

    @Override
    public void updateUserProperties(Properties properties) {
        try {
            Path path = propertiesLoader.getResourceFromExternal(USER_PROPERTIES_FILE);
            Properties userProps = propertiesLoader.getPropertiesFromFile(path);
            userProps.putAll(properties);
            propertiesLoader.savePropertiesToFile(path, userProps);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
