package com.gitlab.genious_mind.gui_java.impl.filesystem;

import lombok.extern.log4j.Log4j2;

import javax.inject.Singleton;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.Collections;
import java.util.Properties;
import java.util.stream.Stream;

@Log4j2
@Singleton
public class PropertiesLoader implements Closeable, AutoCloseable {

    private static final String JAR_SCHEMA = "jar";
    private final FileSystem fileSystem;

    public PropertiesLoader() {
        this.fileSystem = getFileSystem();
    }

    private static FileSystem getFileSystem() {
        try {
            URI uri = PropertiesLoader.class.getResource("").toURI();
            if (JAR_SCHEMA.equalsIgnoreCase(uri.getScheme())) {
                return FileSystems.newFileSystem(uri, Collections.emptyMap());
            }
        } catch (URISyntaxException | IOException e) {
            log.warn("Unable to determine if we are inside a JAR!", e);
        }
        return null;
    }

    public Stream<Path> findAllFilesInClasspathFolder(String folder) throws URISyntaxException, IOException {
            return Files.walk(getResourceFromClasspath(folder)).filter(Files::isRegularFile);
    }

    public Properties getPropertiesFromFile(Path path) throws IOException {
        try (InputStream inputStream = Files.newInputStream(path)) {
            Properties props = new Properties();
            props.load(inputStream);
            return props;
        }
    }

    public Path getResourceFromClasspath(String path) throws URISyntaxException {
        if (fileSystem != null) {
            return fileSystem.getPath(path);
        } else {
            return Paths.get(getClass().getResource(path).toURI());
        }
    }

    public Path getResourceFromExternal(String path) {
        return Paths.get(path);
    }

    @Override
    public void close() throws IOException {
        if(null != fileSystem && fileSystem.isOpen()){
            fileSystem.close();
        }
    }

    public void savePropertiesToFile(Path path, Properties properties) throws IOException {
        properties.store(new FileOutputStream(path.toFile()), null);
    }

}
