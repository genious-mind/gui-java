package com.gitlab.genious_mind.gui_java.impl.gui.swing;

import com.gitlab.genious_mind.gui_java.common.Window;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;

import javax.swing.*;

public abstract class AbstractWindow implements Window {

  protected final JFrame frame;
  private final I18nService i18nService;

  public AbstractWindow(I18nService i18nService) {
    this.i18nService = i18nService;
    this.frame = new JFrame();
  }

  protected void openWindow() {
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.setVisible(true);
    frame.pack();
  }

  protected String translate(String key, Object... args) {
    return i18nService.getString(key, args);
  }

  @Override
  public void close() {
    frame.setVisible(false);
    frame.dispose();
  }

  @Override
  public void showServerDisconnectionError() {
    showError(translate("common.message.disconnection.title"), translate("common.message.disconnection.message"));
  }

  @Override
  public void showInfo(String title, String message) {
    JOptionPane.showMessageDialog(frame, message, title, JOptionPane.INFORMATION_MESSAGE);
  }

  @Override
  public void showWarning(String title, String message) {
    JOptionPane.showMessageDialog(frame, message, title, JOptionPane.WARNING_MESSAGE);
  }

  @Override
  public void showError(String title, String message) {
    JOptionPane.showMessageDialog(frame, message, title, JOptionPane.ERROR_MESSAGE);
  }

  @Override
  public String showPrompt(String title, String message) {
    return JOptionPane.showInputDialog(frame, message, title, JOptionPane.QUESTION_MESSAGE);
  }

  @Override
  public boolean showConfirm(String title, String message) {
    int confirm = JOptionPane.showConfirmDialog(frame, message, title, JOptionPane.YES_NO_OPTION);
    return (confirm == JOptionPane.YES_OPTION);
  }

}
