package com.gitlab.genious_mind.gui_java.impl.gui.swing;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.ObjIntConsumer;

public abstract class GenericTableModel<T> extends AbstractTableModel {

    private final transient List<T> elementList;
    private final String[] columnNames;

    public GenericTableModel(String[] columnNames) {
        this.columnNames = columnNames;
        this.elementList = new ArrayList<>();
    }

    public T getRowAt(int selectedIndex) {
        return elementList.get(selectedIndex);
    }

    public void addAll(Collection<T> collection) {
        elementList.clear();
        elementList.addAll(collection);
    }

    protected String[] getColumnNames() {
        return columnNames;
    }

    protected abstract Class<?>[] getColumnTypes();

    @Override
    public String getColumnName(int column) {
        return getColumnNames()[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getColumnTypes()[columnIndex];
    }

    @Override
    public int getColumnCount() {
        return getColumnNames().length;
    }

    @Override
    public int getRowCount() {
        return elementList.size();
    }

    public static JTable createJTable(AbstractTableModel tableModel, ObjIntConsumer<Integer> listener, boolean isEnabled) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);

        JTable table = new JTable();
        table.setModel(tableModel);
        table.getSelectionModel().addListSelectionListener(e -> {
            if (listener != null) {
                listener.accept(e.getFirstIndex(), e.getLastIndex());
            }
        });
        table.setDefaultRenderer(String.class, centerRenderer);
        table.setDefaultRenderer(Integer.class, centerRenderer);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setEnabled(isEnabled);
        return table;
    }

    public static JPanel createScrollableTablePanel(JTable table) {
        JPanel panel = new JPanel();
        panel.add(new JScrollPane(table));
        return panel;
    }

}