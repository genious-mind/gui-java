package com.gitlab.genious_mind.gui_java.impl.gui.swing.configuration;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.configuration.ConfigurationEventHandler;
import com.gitlab.genious_mind.gui_java.states.configuration.ConfigurationWindow;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;

import javax.inject.Inject;
import javax.swing.*;

public class ConfigurationSwing extends AbstractWindow implements ConfigurationWindow {

    private ConfigurationEventHandler eventHandler;
    private Slider numberLengthSlider;
    private Slider maxDigitSlider;

    @Inject
    public ConfigurationSwing(I18nService i18NService) {
        super(i18NService);
    }

    @Override
    public void setEventHandler(ConfigurationEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    @Override
    public void open(int minMaxDigit, int maxMaxDigit, int minNumberLength, int maxNumberLength, MastermindConfig config) {
        numberLengthSlider = new Slider(translate("configuration.number.length.text"), minNumberLength, maxNumberLength, this::onNumberLengthChange);
        maxDigitSlider = new Slider(translate("configuration.max.digit.text"), minMaxDigit, maxMaxDigit, this::onMaxDigitChange);
        numberLengthSlider.updateSliderValue(config.getNumberLength());
        maxDigitSlider.updateSliderValue(config.getMaxDigit());

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS));
        frame.add(new JLabel(translate("configuration.title")));
        frame.add(numberLengthSlider);
        frame.add(maxDigitSlider);
        frame.add(createButtonsPanel(eventHandler::onStart, eventHandler::onCancel));
        openWindow();
    }

    private void onNumberLengthChange(Integer changedValue) {
        MastermindConfig config = eventHandler.onNumberLengthChange(changedValue);
        numberLengthSlider.updateSliderValue(config.getNumberLength());
        maxDigitSlider.updateSliderValue(config.getMaxDigit());
    }

    private void onMaxDigitChange(Integer changedValue) {
        MastermindConfig config = eventHandler.onMaxDigitChange(changedValue);
        numberLengthSlider.updateSliderValue(config.getNumberLength());
        maxDigitSlider.updateSliderValue(config.getMaxDigit());
    }

    private JPanel createButtonsPanel(Runnable connectFunction, Runnable cancelFunction) {
        JButton connectionButton = new JButton();
        connectionButton.setText(translate("common.button.start"));
        connectionButton.addActionListener(e -> connectFunction.run());

        JButton cancelButton = new JButton();
        cancelButton.setText(translate("common.button.cancel"));
        cancelButton.addActionListener(e -> cancelFunction.run());

        JPanel panel = new JPanel();
        panel.add(cancelButton);
        panel.add(connectionButton);
        return panel;
    }

}
