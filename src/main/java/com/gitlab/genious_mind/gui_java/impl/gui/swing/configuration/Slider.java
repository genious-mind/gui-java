package com.gitlab.genious_mind.gui_java.impl.gui.swing.configuration;

import javax.swing.*;
import java.util.Hashtable;
import java.util.function.Consumer;

public class Slider extends JPanel {

    private final JSlider slider;
    private final JTextField display;

    public Slider(String text, int minValue, int maxValue, Consumer<Integer> onChange) {
        this.slider = createSlider(minValue, maxValue, onChange);
        this.display = createDisplay();

        add(new JLabel(text + ": "));
        add(slider);
        add(display);
    }

    public void updateSliderValue(int value) {
        slider.setValue(value);
        display.setText(Integer.toString(value));
    }

    private static JTextField createDisplay() {
        JTextField field = new JTextField();
        field.setEnabled(false);
        field.setEditable(false);
        field.setColumns(3);
        return field;
    }

    private static JSlider createSlider(int min, int max, Consumer<Integer> listener) {
        JSlider slider = new JSlider();
        slider.setMinimum(min);
        slider.setMaximum(max);
        slider.setPaintLabels(true);
        slider.setLabelTable(new Hashtable<Integer, JLabel>() {{
            put(min, new JLabel(Integer.toString(min)));
            put((max-min)/2, new JLabel(Integer.toString((max-min)/2)));
            put(max, new JLabel(Integer.toString(max)));
        }});
        slider.addChangeListener(e -> listener.accept(slider.getValue()));

        return slider;
    }

}
