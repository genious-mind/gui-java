package com.gitlab.genious_mind.gui_java.impl.gui.swing.direct.discovery;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.direct.DirectConnectionEventHandler;
import com.gitlab.genious_mind.gui_java.states.direct.DirectConnectionWindow;

import javax.inject.Inject;
import javax.swing.*;
import java.awt.*;

public class DirectConnectionSwing extends AbstractWindow implements DirectConnectionWindow {

  private DirectConnectionEventHandler eventHandler;
  private final JTextField addressField;
  private final JTextField portField;

  @Inject
  public DirectConnectionSwing(I18nService i18NService) {
    super(i18NService);
    addressField = new JTextField( 30);
    portField = new JTextField(30);
  }

  @Override
  public void setEventHandler(DirectConnectionEventHandler directConnectionEventHandler) {
    this.eventHandler = directConnectionEventHandler;
  }

  @Override
  public void showInvalidPortNumberError(String port) {
    showError(translate("connect.direct.error.port.title"), translate("connect.direct.error.port.message", port));
  }

  @Override
  public void showInvalidAddressError(String address) {
    showError(translate("connect.direct.error.address.title"), translate("connect.direct.error.address.message", address));
  }

  @Override
  public void open() {
    frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS));
    frame.add(createPanel(addressField, "connect.direct.text.address"));
    frame.add(createPanel(portField, "connect.direct.text.port"));
    frame.add(createButtonsPanel(this::onConnect, eventHandler::onCancel));

    openWindow();
  }

  private Component createPanel(JTextField text, String key) {
    JPanel panel = new JPanel(new BorderLayout());
    panel.add(new JLabel(translate(key)), BorderLayout.WEST);
    panel.add(text, BorderLayout.EAST);
    return panel;
  }

  private void onConnect() {
    eventHandler.onConnect(addressField.getText(), portField.getText());
  }

  private JPanel createButtonsPanel(Runnable connectFunction, Runnable cancelFunction) {
    JButton connectionButton = new JButton();
    connectionButton.setText(translate("common.button.connect"));
    connectionButton.addActionListener(e -> connectFunction.run());

    JButton cancelButton = new JButton();
    cancelButton.setText(translate("common.button.cancel"));
    cancelButton.addActionListener(e -> cancelFunction.run());

    JPanel panel = new JPanel();
    panel.add(cancelButton);
    panel.add(connectionButton);
    return panel;
  }

}
