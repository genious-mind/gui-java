package com.gitlab.genious_mind.gui_java.impl.gui.swing.discovery;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.GenericTableModel;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.discoverer.ServerDiscovererEventHandler;
import com.gitlab.genious_mind.gui_java.states.discoverer.ServerDiscovererWindow;

import javax.inject.Inject;
import javax.swing.*;
import java.util.Objects;
import java.util.Set;

public class ServerDiscovererSwing extends AbstractWindow implements ServerDiscovererWindow {

  private final ServerTableModel tableModel;
  private ServerDiscovererEventHandler discoveryEventHandler;
  private Packet serverInfo;
  private JButton connectionButton;

  @Inject
  public ServerDiscovererSwing(I18nService i18NService) {
    super(i18NService);
    this.tableModel = new ServerTableModel(new String[]{
            translate("common.connection.address"),
            translate("common.connection.port"),
            translate("discover.table.column.name"),
    });
  }

  @Override
  public void setEventHandler(ServerDiscovererEventHandler discoveryEventHandler) {
    this.discoveryEventHandler = discoveryEventHandler;
  }

  @Override
  public void open() {
    JTable table = GenericTableModel.createJTable(tableModel, this::onSelection, true);
    frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS));
    frame.add(GenericTableModel.createScrollableTablePanel(table));
    frame.add(createButtonsPanel(this::onConnect, discoveryEventHandler::onCancel));

    openWindow();
  }

  private void onSelection(Integer firstIndex, Integer lastIndex) {
    if (lastIndex != null && Objects.equals(firstIndex, lastIndex)) {
      this.serverInfo = this.tableModel.getRowAt(firstIndex);
    } else {
      this.serverInfo = null;
    }
    connectionButton.setEnabled(serverInfo != null);
  }

  private void onConnect() {
    discoveryEventHandler.onConnect(serverInfo);
  }

  private JPanel createButtonsPanel(Runnable connectFunction, Runnable cancelFunction) {
    connectionButton = new JButton();
    connectionButton.setText(translate("common.button.connect"));
    connectionButton.setEnabled(false);
    connectionButton.addActionListener(e -> connectFunction.run());

    JButton cancelButton = new JButton();
    cancelButton.setText(translate("common.button.cancel"));
    cancelButton.addActionListener(e -> cancelFunction.run());

    JPanel panel = new JPanel();
    panel.add(cancelButton);
    panel.add(connectionButton);
    return panel;
  }

  @Override
  public void updateServerList(Set<Packet> servers) {
    tableModel.addAll(servers);
    tableModel.fireTableDataChanged();
  }

}
