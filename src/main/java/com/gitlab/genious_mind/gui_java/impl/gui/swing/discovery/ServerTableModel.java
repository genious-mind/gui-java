package com.gitlab.genious_mind.gui_java.impl.gui.swing.discovery;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.GenericTableModel;

public class ServerTableModel extends GenericTableModel<Packet> {

    public ServerTableModel(String[] columnNames) {
        super(columnNames);
    }

    @Override
    protected Class<?>[] getColumnTypes() {
        return new Class[]{String.class, Integer.class, String.class};
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Packet row = getRowAt(rowIndex);
        switch (columnIndex) {
            case 0:
                return row.getAddress();
            case 1:
                return row.getPort();
            case 2:
                return row.getMessage();
            default:
                return null;
        }
    }

}