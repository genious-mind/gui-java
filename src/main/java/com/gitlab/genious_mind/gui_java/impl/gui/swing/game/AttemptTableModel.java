package com.gitlab.genious_mind.gui_java.impl.gui.swing.game;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.GenericTableModel;
import com.gitlab.genious_mind.gui_java.util.core.MastermindAttempt;

public class AttemptTableModel extends GenericTableModel<MastermindAttempt> {

    public AttemptTableModel(String[] columnNames) {
        super(columnNames);
    }

    @Override
    protected Class<?>[] getColumnTypes() {
        return new Class[]{String.class, Integer.class, Integer.class, String.class};
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        MastermindAttempt row = getRowAt(rowIndex);
        switch (columnIndex) {
            case 0:
                return DisplayNumber.formatAttempt(row.getAttempt());
            case 1:
                return row.getDigitsExact();
            case 2:
                return row.getDigitsPresent();
            case 3:
                return DisplayTimer.formatTime(row.getElapsedSeconds());
            default:
                return null;
        }
    }

}