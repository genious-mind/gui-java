package com.gitlab.genious_mind.gui_java.impl.gui.swing.game;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DisplayNumber extends JTextField {

    private static final String DELIMITER = " - ";

    public DisplayNumber() {
        setEnabled(false);
        setEditable(false);
        setSize(new Dimension(1000, 1000));
        setAttempt(Collections.emptyList());
    }

    public void setAttempt(Collection<Integer> attempt) {
        setText(formatAttempt(attempt));
    }

    public static String formatAttempt(int[] attempt) {
        return formatAttempt(Arrays.stream(attempt));
    }

    public static String formatAttempt(Collection<Integer> attempt) {
        return formatAttempt(attempt.stream().mapToInt(Integer::intValue));
    }

    private static String formatAttempt(IntStream attempt) {
        return attempt.mapToObj(String::valueOf).collect(Collectors.joining(DELIMITER));
    }

}
