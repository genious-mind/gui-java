package com.gitlab.genious_mind.gui_java.impl.gui.swing.game;

import javax.swing.*;

import static java.util.concurrent.TimeUnit.*;

public class DisplayTimer extends JTextField {

    private static final String TIME_FORMAT = "%02d:%02d:%02d";
    private static final int INITIAL_TIME = 0;

    public DisplayTimer() {
        setEnabled(false);
        setEditable(false);
        update(INITIAL_TIME);
    }

    public void update(long time) {
        setText(formatTime(time));
    }

    public static String formatTime(long time) {
        long hours = SECONDS.toHours(time);
        long minutes = SECONDS.toMinutes(time) - HOURS.toMinutes(hours);
        long seconds = SECONDS.toSeconds(time) - HOURS.toSeconds(hours) - MINUTES.toSeconds(minutes);
        return String.format(TIME_FORMAT, hours, minutes, seconds);
    }

}
