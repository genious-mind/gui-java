package com.gitlab.genious_mind.gui_java.impl.gui.swing.game;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.GenericTableModel;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.game.client.GameEventHandler;
import com.gitlab.genious_mind.gui_java.states.game.client.GameWindow;
import com.gitlab.genious_mind.gui_java.util.core.MastermindAttempt;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;

import javax.inject.Inject;
import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GameSwing extends AbstractWindow implements GameWindow {

    private GameEventHandler gameEventHandler;
    private final AttemptTableModel model;
    private final DisplayNumber number;
    private final boolean isSinglePlayer;
    private final DisplayTimer timer;

    @Inject
    public GameSwing(I18nService i18nService, boolean isSinglePlayer) {
        super(i18nService);
        this.isSinglePlayer = isSinglePlayer;
        this.model = new AttemptTableModel(new String[]{
                translate("game.table.column.attempt"),
                translate("game.table.column.exact"),
                translate("game.table.column.present"),
                translate("game.table.column.time")
        });
        this.number = new DisplayNumber();
        this.timer = new DisplayTimer();
    }

    @Override
    public void setEventHandler(GameEventHandler gameEventHandler) {
        this.gameEventHandler = gameEventHandler;
    }

    @Override
    public void open(MastermindConfig config) {
        JTable table = GenericTableModel.createJTable(model, null, false);
        frame.setLayout(new BorderLayout());
        frame.add(createGamePanel(config), BorderLayout.WEST);
        frame.add(GenericTableModel.createScrollableTablePanel(table), BorderLayout.EAST);
        frame.add(createExitPanel(gameEventHandler::onExit), BorderLayout.SOUTH);

        openWindow();
    }

    private Component createGamePanel(MastermindConfig config) {
        JPanel frame = new JPanel();
        frame.setLayout(new BoxLayout(frame, BoxLayout.PAGE_AXIS));
        frame.add(timer);
        frame.add(createDisplayPanel(number));
        frame.add(createButtonsPanel(config.getMinDigit(), config.getMaxDigit() + 1, gameEventHandler::onPressNumber));
        frame.add(createButtonsPanel(gameEventHandler::onDelete, gameEventHandler::onSend));
        return frame;
    }

    @Override
    public void updateDisplay(Collection<Integer> attempt) {
        number.setAttempt(attempt);
    }

    @Override
    public void updateAttemptList(List<MastermindAttempt> attemptList) {
        model.addAll(attemptList);
        model.fireTableDataChanged();
    }

    @Override
    public void disableInterface() {
        disableJPanel(frame.getContentPane());
    }

    @Override
    public boolean showExitConfirmation() {
        String singleMessage = "game.message.exit.confirm.single.player.message";
        String multiMessage = "game.message.exit.confirm.multi.player.message";
        String message = translate(isSinglePlayer ? singleMessage : multiMessage);
        return showConfirm(translate("game.message.exit.confirm.title"), message);
    }

    @Override
    public void showVictoryMessage(Long score) {
        String singleMessage = "game.message.victory.single.player.message";
        String multiMessage = "game.message.victory.multi.player.message";
        showInfo(translate("game.message.victory.title"), translate(isSinglePlayer ? singleMessage : multiMessage, score));
    }

    @Override
    public void showPlayerDisconnectedError(String playerName) {
        showWarning(translate("game.message.player.disconnected.title"), translate("game.message.player.disconnected.message", playerName));
    }

    @Override
    public void updateTime(long time) {
        this.timer.update(time);
    }

    @Override
    public void showDefeatMessage(int[] number) {
        String attempt = DisplayNumber.formatAttempt(number);
        showInfo(translate("game.message.defeat.title"), translate("game.message.defeat.message", attempt));
    }

    private static void disableJPanel(Component component) {
        component.setEnabled(false);

        if (component instanceof Container) {
            Container container = (Container) component;
            Arrays.stream(container.getComponents()).forEach(GameSwing::disableJPanel);
        }
    }

    private JPanel createButtonsPanel(Runnable onDelete, Runnable onSend) {
        JButton deleteButton = new JButton();
        deleteButton.setText(translate("game.button.delete"));
        deleteButton.addActionListener(e -> onDelete.run());

        JButton sendButton = new JButton();
        sendButton.setText(translate("game.button.send"));
        sendButton.addActionListener(e -> onSend.run());

        JPanel panel = new JPanel();
        panel.add(sendButton);
        panel.add(deleteButton);
        return panel;
    }

    private static JPanel createDisplayPanel(JTextField display) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 1));
        panel.setBackground(Color.RED);
        panel.add(display);
        return panel;
    }

    private static List<JButton> createButtons(int from, int to, IntConsumer function) {
        return IntStream.range(from, to).mapToObj(n -> createButton(n, function)).collect(Collectors.toList());
    }

    private static JButton createButton(int number, IntConsumer function) {
        JButton button = new JButton();
        button.setText(Integer.toString(number));
        button.addActionListener(event -> function.accept(number));
        return button;
    }

    private JPanel createExitPanel(Runnable connectFunction) {
        JButton exitButton = new JButton();
        exitButton.setText(translate("game.button.exit"));
        exitButton.addActionListener(e -> connectFunction.run());

        JPanel panel = new JPanel();
        panel.add(exitButton);
        return panel;
    }

    private static JPanel createButtonsPanel(int from, int to, IntConsumer function) {
        List<JButton> buttons = createButtons(from, to, function);

        JPanel panel = new JPanel();
        panel.setLayout(createButtonsLayout(buttons.size()));
        buttons.forEach(panel::add);
        return panel;
    }

    private static LayoutManager createButtonsLayout(long buttonsCount) {
        int gridSize = getGridSize(buttonsCount);
        return new GridLayout(gridSize, gridSize);
    }

    private static int getGridSize(long buttonsCount) {
        return (int) Math.ceil(Math.sqrt(buttonsCount));
    }

}
