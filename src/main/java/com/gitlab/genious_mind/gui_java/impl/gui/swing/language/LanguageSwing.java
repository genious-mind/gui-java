package com.gitlab.genious_mind.gui_java.impl.gui.swing.language;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.language.LanguageSelectionEventHandler;
import com.gitlab.genious_mind.gui_java.states.language.LanguageSelectionWindow;

import javax.inject.Inject;
import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.Locale;
import java.util.stream.Collectors;

public class LanguageSwing extends AbstractWindow implements LanguageSelectionWindow {

  private LanguageSelectionEventHandler eventHandler;
  private Collection<Locale> availableLanguages;
  private Locale currentLocale;
  private JPanel buttonPanel;

  @Inject
  public LanguageSwing(I18nService i18NService) {
    super(i18NService);
  }

  @Override
  public void setEventHandler(LanguageSelectionEventHandler eventHandler) {
    this.eventHandler = eventHandler;
  }

  @Override
  public void open(Collection<Locale> availableLanguages, Locale currentLocale) {
    this.availableLanguages = availableLanguages;
    this.currentLocale = currentLocale;
    this.buttonPanel = new JPanel(new GridLayout((availableLanguages.size() + 1) / 2, 2));
    createLanguagesButton(availableLanguages, currentLocale).forEach(buttonPanel::add);

    frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS));
    frame.add(new JLabel(translate("language.title")));
    frame.add(buttonPanel);
    frame.add(createButtonsPanel(this::onSave, eventHandler::onCancel));

    openWindow();
  }

  private Collection<Component> createLanguagesButton(Collection<Locale> availableLanguages, Locale currentLocale) {
    return availableLanguages.stream().map(locale -> createButton(locale, currentLocale)).collect(Collectors.toList());
  }

  private Component createButton(Locale locale, Locale currentLocale) {
    JButton button = new JButton();
    button.setText(locale.getDisplayLanguage().toUpperCase());
    button.addActionListener(e -> updateLocale(locale));
    button.setEnabled(!locale.getISO3Language().equals(currentLocale.getISO3Language()));
    return button;
  }

  private void updateLocale(Locale locale) {
    this.currentLocale = locale;

    buttonPanel.removeAll();
    createLanguagesButton(availableLanguages, currentLocale).forEach(buttonPanel::add);

    frame.revalidate();
  }

  private void onSave() {
    eventHandler.onSave(currentLocale);
  }

  private JPanel createButtonsPanel(Runnable connectFunction, Runnable cancelFunction) {
    JButton connectionButton = new JButton();
    connectionButton.setText(translate("language.button.save"));
    connectionButton.addActionListener(e -> connectFunction.run());

    JButton cancelButton = new JButton();
    cancelButton.setText(translate("common.button.cancel"));
    cancelButton.addActionListener(e -> cancelFunction.run());

    JPanel panel = new JPanel();
    panel.add(cancelButton);
    panel.add(connectionButton);
    return panel;
  }

}
