package com.gitlab.genious_mind.gui_java.impl.gui.swing.lobby;

import java.awt.FlowLayout;
import java.util.Collection;

import javax.swing.*;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.GenericTableModel;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.discovery.ServerTableModel;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.lobby.client.ClientLobbyEventHandler;
import com.gitlab.genious_mind.gui_java.states.lobby.client.ClientLobbyWindow;
import com.gitlab.genious_mind.gui_java.states.lobby.server.ServerLobbyEventHandler;
import com.gitlab.genious_mind.gui_java.states.lobby.server.ServerLobbyWindow;

public class ServerLobbySwing extends AbstractWindow implements ServerLobbyWindow, ClientLobbyWindow {

    private final boolean isClient;
    private final ServerTableModel model;
    private ClientLobbyEventHandler clientEventHandler;
    private ServerLobbyEventHandler serverEventHandler;

    public ServerLobbySwing(I18nService i18NService, boolean isClient) {
        super(i18NService);
        this.isClient = isClient;
        this.model = new ServerTableModel(new String[]{
                translate("common.connection.address"),
                translate("common.connection.port"),
                translate("lobby.table.column.name"),
        });
    }

    @Override
    public void setClientEventHandler(ClientLobbyEventHandler clientEventHandler) {
        this.clientEventHandler = clientEventHandler;
    }

    @Override
    public void setEventHandler(ServerLobbyEventHandler eventHandler) {
        this.serverEventHandler = eventHandler;
        this.clientEventHandler = eventHandler;
    }

    @Override
    public void showConnectionError(String address, int port) {
        String title = translate("lobby.massage.connection.error.title");
        String message = translate("lobby.client.message.connection.error.message", address, port);
        showError(title, message);
    }

    @Override
    public void showConnectionError() {
        String title = translate("lobby.massage.connection.error.title");
        String message = translate("lobby.server.message.connection.error.message");
        showError(title, message);
    }

    @Override
    public void showMaxPlayerReachedError() {
        showError(translate("common.message.disconnection.title"), translate("lobby.message.max.player.error.message"));
    }

    @Override
    public void showGameAlreadyStarted() {
        showError(translate("common.message.disconnection.title"), translate("lobby.message.already.started.error.message"));
    }

    @Override
    public void open(Packet packet) {
        JTable table = GenericTableModel.createJTable(model, null, false);

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS));
        frame.add(createInfoPanel(packet));
        frame.add(GenericTableModel.createScrollableTablePanel(table));
        frame.add(createButtonsPanel());
        openWindow();
    }

    private JPanel createInfoPanel(Packet serverInfo) {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(new JLabel(translate("common.connection.address") + ":"));
        panel.add(createTextField(serverInfo.getAddress()));
        panel.add(new JLabel(translate("common.connection.port") + ":"));
        panel.add(createTextField(Integer.toString(serverInfo.getPort())));
        panel.add(new JLabel(translate("lobby.info.server.name") + ":"));
        panel.add(createTextField(serverInfo.getMessage()));
        return panel;
    }

    private static JTextField createTextField(String text) {
        JTextField field = new JTextField();
        field.setText(text);
        field.setEditable(false);
        return field;
    }

    private JPanel createButtonsPanel() {
        JButton connectionButton = new JButton();
        connectionButton.setText(translate("common.button.start"));
        connectionButton.setVisible(!isClient);
        connectionButton.addActionListener(e -> serverEventHandler.onStart());

        JButton cancelButton = new JButton();
        cancelButton.setText(translate("common.button.cancel"));
        cancelButton.addActionListener(e -> clientEventHandler.onCancel());

        JPanel panel = new JPanel();
        panel.add(cancelButton);
        panel.add(connectionButton);
        return panel;
    }

    @Override
    public String askServerName() {
        String title = translate("lobby.massage.ask.name.title");
        String message = translate("lobby.message.ask.name.message") + ":";
        return showPrompt(title, message);
    }

    @Override
    public void updateClients(Collection<Packet> clients) {
        model.addAll(clients);
        model.fireTableDataChanged();
    }

}
