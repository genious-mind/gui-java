package com.gitlab.genious_mind.gui_java.impl.gui.swing.menu;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.menu.main.MainMenuEventHandler;
import com.gitlab.genious_mind.gui_java.states.menu.main.MainMenuWindow;

import javax.inject.Inject;
import javax.swing.*;
import java.awt.*;

public class MainMenuSwing extends AbstractWindow implements MainMenuWindow {

    private MainMenuEventHandler eventHandler;

    @Inject
    public MainMenuSwing(I18nService i18NService) {
        super(i18NService);
    }

    @Override
    public void open() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.PAGE_AXIS));
        jPanel.add(createButton(translate("menu.main.single.player"), eventHandler::onSinglePlayer));
        jPanel.add(createButton(translate("menu.main.multi.player"), eventHandler::onMultiPlayer));
        jPanel.add(createButton(translate("menu.main.language"), eventHandler::onLanguage));
        jPanel.add(createButton(translate("menu.main.exit"), eventHandler::onExit));
        frame.setContentPane(jPanel);
        openWindow();
    }

    @Override
    public void setEventHandler(MainMenuEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    public static JButton createButton(String text, Runnable function) {
        return createButton(text, function, true);
    }

    public static JButton createButton(String text, Runnable function, boolean isEnable) {
        JButton button = new JButton();
        button.setText(text);
        button.setEnabled(isEnable);
        button.addActionListener(e -> function.run());
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        return button;
    }

}
