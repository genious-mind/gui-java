package com.gitlab.genious_mind.gui_java.impl.gui.swing.menu;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.menu.multiplayer.MultiPlayerMenuEventHandler;
import com.gitlab.genious_mind.gui_java.states.menu.multiplayer.MultiPlayerMenuWindow;

import javax.inject.Inject;
import javax.swing.*;

import static com.gitlab.genious_mind.gui_java.impl.gui.swing.menu.MainMenuSwing.createButton;

public class MultiPlayerMenuSwing extends AbstractWindow implements MultiPlayerMenuWindow {

    private MultiPlayerMenuEventHandler eventHandler;

    @Inject
    public MultiPlayerMenuSwing(I18nService i18NService) {
        super(i18NService);
    }

    @Override
    public void open() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.PAGE_AXIS));
        jPanel.add(createButton(translate("menu.multi.player.discover"), eventHandler::onDiscoverServer));
        jPanel.add(createButton(translate("menu.multi.player.direct"), eventHandler::onDirectConnection));
        jPanel.add(createButton(translate("menu.multi.player.lobby"), eventHandler::onCreateLobby));
        jPanel.add(createButton(translate("common.button.back"), eventHandler::onGoBack));
        frame.setContentPane(jPanel);
        openWindow();
    }

    @Override
    public void setEventHandler(MultiPlayerMenuEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

}
