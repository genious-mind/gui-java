package com.gitlab.genious_mind.gui_java.impl.gui.swing.menu;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.menu.singleplayer.SinglePlayerMenuEventHandler;
import com.gitlab.genious_mind.gui_java.states.menu.singleplayer.SinglePlayerMenuWindow;

import javax.inject.Inject;
import javax.swing.*;

import java.awt.*;

import static com.gitlab.genious_mind.gui_java.impl.gui.swing.menu.MainMenuSwing.createButton;

public class SinglePlayerMenuSwing extends AbstractWindow implements SinglePlayerMenuWindow {

    private SinglePlayerMenuEventHandler eventHandler;

    @Inject
    public SinglePlayerMenuSwing(I18nService i18NService) {
        super(i18NService);
    }

    @Override
    public void open(boolean isContinueEnabled) {

        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.PAGE_AXIS));
        jPanel.add(createButton(translate("menu.single.player.continue"), eventHandler::onContinue, isContinueEnabled));
        jPanel.add(createButton(translate("menu.single.player.new"), eventHandler::onNewMatch));
        jPanel.add(createButton(translate("menu.single.player.record"), eventHandler::onRecord));
        jPanel.add(createButton(translate("common.button.back"), eventHandler::onGoBack));
        frame.setContentPane(jPanel);
        openWindow();
    }

    @Override
    public void setEventHandler(SinglePlayerMenuEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

}
