package com.gitlab.genious_mind.gui_java.impl.gui.swing.record;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.AbstractWindow;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.GenericTableModel;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.states.record.RecordEventHandler;
import com.gitlab.genious_mind.gui_java.states.record.RecordWindow;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;

import javax.inject.Inject;
import javax.swing.*;
import java.awt.*;
import java.util.Collection;

public class RecordSwing extends AbstractWindow implements RecordWindow {

  private RecordEventHandler eventHandler;

  @Inject
  public RecordSwing(I18nService i18NService) {
    super(i18NService);
  }

  @Override
  public void setEventHandler(RecordEventHandler eventHandler) {
    this.eventHandler = eventHandler;
  }

  @Override
  public void open(Collection<Mastermind> matches) {
    RecordTableModel recordTableModel = new RecordTableModel(new String[]{
            translate("record.table.column.date"),
            translate("record.table.column.max.digit"),
            translate("record.table.column.number.length"),
            translate("record.table.column.attempt.count"),
            translate("record.table.column.elapsed.time"),
            translate("record.table.column.score"),
    });
    recordTableModel.addAll(matches);
    JTable table = GenericTableModel.createJTable(recordTableModel, null, false);

    frame.add(GenericTableModel.createScrollableTablePanel(table), BorderLayout.NORTH);
    frame.add(createButtonPanel(eventHandler::onGoBack), BorderLayout.SOUTH);

    openWindow();
  }

  private JPanel createButtonPanel(Runnable connectFunction) {
    JButton onGoBackButton = new JButton();
    onGoBackButton.setText(translate("common.button.back"));
    onGoBackButton.addActionListener(e -> connectFunction.run());

    JPanel panel = new JPanel();
    panel.add(onGoBackButton);
    return panel;
  }

}
