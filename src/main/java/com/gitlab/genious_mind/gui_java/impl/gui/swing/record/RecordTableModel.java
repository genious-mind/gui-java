package com.gitlab.genious_mind.gui_java.impl.gui.swing.record;

import com.gitlab.genious_mind.gui_java.impl.gui.swing.GenericTableModel;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.game.DisplayTimer;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class RecordTableModel extends GenericTableModel<Mastermind> {

    private static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

    public RecordTableModel(String[] columnNames) {
        super(columnNames);
    }

    @Override
    protected Class<?>[] getColumnTypes() {
        return new Class[]{String.class, Integer.class, Integer.class, Integer.class, String.class, Long.class};
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Mastermind row = getRowAt(rowIndex);
        switch (columnIndex) {
            case 0:
                return formatToDate(row.getStartTime());
            case 1:
                return row.getConfig().getMaxDigit();
            case 2:
                return row.getConfig().getNumberLength();
            case 3:
                return row.getAttempts().size();
            case 4:
                return DisplayTimer.formatTime(row.getElapsedSeconds());
            case 5:
                return row.getScore();
            default:
                return null;
        }
    }

    private static String formatToDate(long time) {
        ZonedDateTime date = ZonedDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        return formatter.format(date);
    }


}