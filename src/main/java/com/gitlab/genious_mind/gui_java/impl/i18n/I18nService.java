package com.gitlab.genious_mind.gui_java.impl.i18n;

import com.gitlab.genious_mind.gui_java.impl.filesystem.PropertiesLoader;
import com.gitlab.genious_mind.gui_java.util.LanguageService;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.gitlab.genious_mind.gui_java.states.language.LanguageSelection.LANGUAGE_PROPERTY;

@Log4j2
@Singleton
public class I18nService implements LanguageService {

    private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
    private static final String LOCALE_REGEX = ".*locale_([a-z]{2})\\.properties";
    private static final String LOCALE_FOLDER = "/locale";

    private final Map<Locale, Properties> languages;
    private Locale language;

    @Inject
    public I18nService(PropertiesLoader propertiesLoader, Locale defaultLocale,
                       @Named(LANGUAGE_PROPERTY) String userLocale) throws IOException, URISyntaxException {
        this.languages = getLanguagesFromProperties(propertiesLoader);
        this.language = getDefaultLanguage(defaultLocale, userLocale);
    }

    private Map<Locale, Properties> getLanguagesFromProperties(PropertiesLoader propertiesLoader) throws IOException, URISyntaxException {

        try {
            Map<Locale, List<Path>> localePaths = propertiesLoader.findAllFilesInClasspathFolder(LOCALE_FOLDER)
                    .filter(I18nService::isValidFilename)
                    .filter(path -> isValidLocale(getLocaleFromString(path)))
                    .sorted(Comparator.comparing(Path::toString))
                    .collect(Collectors.groupingBy(I18nService::getLocaleFromString));

            return getPropertiesMap(propertiesLoader, localePaths);
        } catch (Exception e) {
            log.error(e);
            throw e;
        }
    }

    private Map<Locale, Properties> getPropertiesMap(PropertiesLoader propertiesLoader, Map<Locale, List<Path>> localeMap) {
        Map<Locale, Properties> map = new HashMap<>();

        localeMap.forEach((locale, paths) -> {
            if (paths.size() > 1) {
                log.warn("More than one file was found for the locale '{}': {}", locale, paths);
            }

            Path path = paths.stream().min(
                    Comparator.comparing(p -> p.toString().length())
                            .thenComparing(Object::toString, Comparator.reverseOrder())
            ).orElse(null);

            try {
                map.put(locale, propertiesLoader.getPropertiesFromFile(path));
            } catch (IOException e) {
                log.warn("Cannot read properties file '{}'!", path, e);
            }
        });

        return map;
    }

    private Locale getDefaultLanguage(Locale defaultLocale, String userLocale) {
        Locale locale = Locale.forLanguageTag(userLocale);
        if (isValidLocale(locale) && isLocaleAvailable(locale)) {
            return locale;
        }
        log.warn("User language '{}' isn't available!", defaultLocale);

        if (isLocaleAvailable(defaultLocale)) {
            return defaultLocale;
        }
        log.warn("Default language '{}' isn't available!", defaultLocale.getLanguage());

        return DEFAULT_LOCALE;
    }

    private boolean isLocaleAvailable(Locale locale) {
        return this.languages.keySet().stream().anyMatch(l -> l.equals(locale));
    }

    private static boolean isValidLocale(Locale locale) {
        try {
            return null != locale && null != locale.getISO3Language();
        } catch (MissingResourceException e) {
            log.warn("Couldn't find 3-letter language code for {}", locale.getLanguage(), e);
            return false;
        }
    }

    private static boolean isValidFilename(Path filename) {
        return filename.toString().toLowerCase().matches(LOCALE_REGEX);
    }

    private static Locale getLocaleFromString(Path filename) {
        Matcher matcher = Pattern.compile(LOCALE_REGEX, Pattern.DOTALL).matcher(filename.toString().toLowerCase());
        if (matcher.find()) {
            return Locale.forLanguageTag(matcher.group(1));
        }

        return null;
    }

    public String getString(String key, Object... args) {
        Properties properties = this.languages.getOrDefault(language, new Properties());
        String string = Optional.ofNullable(properties.getProperty(key)).orElse("");
        return String.format(string, args);
    }

    @Override
    public Collection<Locale> getAvailableLanguages() {
        return languages.keySet();
    }

    @Override
    public Locale getCurrentLanguage() {
        return language;
    }

    @Override
    public void setCurrentLanguage(Locale language) {
        this.language = language;
    }

}
