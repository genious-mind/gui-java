package com.gitlab.genious_mind.gui_java.impl.network.java.discovery;

import com.gitlab.genious_mind.gui_java.network.unreliable.UnreliableConnection;

import javax.inject.Inject;
import javax.inject.Named;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDPClientJava extends UDPConnectionJava implements UnreliableConnection {

  private final int timeout;

  @Inject
  public UDPClientJava(@Named("connection.unreliable.client.timeout.ms") int timeout) {
    this.timeout = timeout;
  }

  @Override
  public boolean createSocket() {
    try {
      if (isSocketClosed()) {
        socket = new DatagramSocket();
        socket.setBroadcast(true);
        socket.setSoTimeout(timeout);
      }
      return true;
    } catch (SocketException e) {
      return false;
    }
  }

}
