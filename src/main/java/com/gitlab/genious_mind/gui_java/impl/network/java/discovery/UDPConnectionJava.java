package com.gitlab.genious_mind.gui_java.impl.network.java.discovery;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.impl.network.java.discovery.exception.SocketClosedException;
import com.gitlab.genious_mind.gui_java.network.unreliable.UnreliableConnection;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public abstract class UDPConnectionJava implements UnreliableConnection {

  protected DatagramSocket socket;

  @Override
  public Packet receiveMessage() throws IOException {
    if (isSocketClosed()) {
      throw new SocketClosedException();
    }

    byte[] recvBuf = new byte[15000];
    DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
    socket.receive(packet);
    String message = new String(packet.getData()).trim();
    return new Packet(packet.getAddress().getHostAddress(), packet.getPort(), message);
  }

  @Override
  public void sendMessage(Packet packet) throws IOException {
    if (isSocketClosed()) {
      throw new SocketClosedException();
    }

    byte[] message = packet.getMessage().getBytes();
    DatagramPacket datagramPacket = new DatagramPacket(message, message.length, InetAddress.getByName(packet.getAddress()), packet.getPort());
    socket.send(datagramPacket);
  }

  @Override
  public boolean closeSocket() {
    try {
      if (!isSocketClosed()) {
        socket.close();
      }
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public boolean isSocketClosed() {
    return (null == socket || socket.isClosed());
  }

}
