package com.gitlab.genious_mind.gui_java.impl.network.java.discovery;

import javax.inject.Inject;
import javax.inject.Named;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDPServerJava extends UDPConnectionJava {

  private final String address;
  private final int port;

  @Inject
  public UDPServerJava(@Named("connection.unreliable.interface.address") String address, @Named("connection.unreliable.server.port") int port) {
    this.address = address;
    this.port = port;
  }

  @Override
  public boolean createSocket() {
    try {
      if (isSocketClosed()) {
        socket = new DatagramSocket(port, InetAddress.getByName(address));
        socket.setBroadcast(true);
      }
      return true;
    } catch (SocketException | UnknownHostException e) {
      return false;
    }
  }

}
