package com.gitlab.genious_mind.gui_java.impl.network.java.discovery.exception;

public class SocketReceiveException extends RuntimeException {

  public SocketReceiveException(Exception e) {
    super(e);
  }

}
