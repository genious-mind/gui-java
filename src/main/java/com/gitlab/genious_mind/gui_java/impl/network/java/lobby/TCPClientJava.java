package com.gitlab.genious_mind.gui_java.impl.network.java.lobby;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientReliableConnection;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.net.Socket;

@Log4j2
public class TCPClientJava implements ClientReliableConnection {

    private ClientMessageReceiver messageReceiver;
    private TCPSocketManager socketHandler;

    @Override
    public void setMessageReceiver(ClientMessageReceiver messageReceiver) {
        this.messageReceiver = messageReceiver;
    }

    @Override
    public void start(Packet packet) throws IOException {
        Socket socket = new Socket(packet.getAddress(), packet.getPort());

        socketHandler = new TCPSocketManager(socket);
        socketHandler.start(new ClientMessageReceiver() {
            @Override
            public void onDisconnection() {
                messageReceiver.onDisconnection();
            }

            @Override
            public void onMessageReceived(String message) {
                messageReceiver.onMessageReceived(message);
            }
        });
    }

    @Override
    public void sendMessage(String message) {
        socketHandler.sendMessage(message);
    }

    @Override
    public void close() {
        socketHandler.close();
    }

    @Override
    public Packet getLocalInfo() {
        return socketHandler.getLocalInfo();
    }

}
