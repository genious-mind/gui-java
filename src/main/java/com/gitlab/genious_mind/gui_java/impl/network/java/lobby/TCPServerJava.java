package com.gitlab.genious_mind.gui_java.impl.network.java.lobby;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.server.ServerMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.server.ServerReliableConnection;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Log4j2
public class TCPServerJava implements ServerReliableConnection {

    private final int port;
    private final Map<Integer, TCPSocketManager> clients;
    private ServerSocket serverSocket;
    private ServerMessageReceiver serverMessageReceiver;

    @Inject
    public TCPServerJava(@Named("connection.reliable.server.port") int port) {
        this.port = port;
        this.clients = new HashMap<>();
    }

    @Override
    public void setMessageReceiver(ServerMessageReceiver serverMessageReceiver) {
        this.serverMessageReceiver = serverMessageReceiver;
    }

    @Override
    public int getPort() {
        return serverSocket.getLocalPort();
    }

    @Override
    public Packet getClientInfo(int clientIndex) {
        return clients.get(clientIndex).getRemoteInfo();
    }

    @Override
    public void start() throws IOException {
        serverSocket = new ServerSocket(port);

        new Thread(() -> {
            try {
                while (null != serverSocket && !serverSocket.isClosed()) {
                    Socket socket = serverSocket.accept();

                    int clientId = calculateClientIndex(clients.keySet());
                    TCPSocketManager client = new TCPSocketManager(socket);
                    client.start(getMessageRecipient(clientId, client));

                    clients.put(clientId, client);
                    serverMessageReceiver.onConnection(clientId);
                }
            } catch (IOException e) {
                log.error(e);
            }
        }).start();
    }

    private static int calculateClientIndex(Set<Integer> indexes) {
        return indexes.stream().mapToInt(Integer::intValue).max().orElse(0) + 1;
    }

    private ClientMessageReceiver getMessageRecipient(int clientId, TCPSocketManager client) {
        return (new ClientMessageReceiver() {
            @Override
            public void onDisconnection() {
                client.close();
                clients.remove(clientId);
                serverMessageReceiver.onDisconnection(clientId);
            }

            @Override
            public void onMessageReceived(String message) {
                serverMessageReceiver.onMessageReceived(clientId, message);
            }
        });
    }

    @Override
    public void sendMessage(String message) {
        clients.forEach((integer, socketManager) -> socketManager.sendMessage(message));
    }

    @Override
    public void sendMessage(String message, int id) {
        clients.get(id).sendMessage(message);
    }

    @Override
    public void close() {
        try {
            this.clients.forEach((integer, socketManager) -> socketManager.close());
            this.clients.clear();
            this.serverSocket.close();
        } catch (Exception e) {
            log.warn("An error occurred while disconnecting server!", e);
        }
    }

}
