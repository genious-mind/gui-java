package com.gitlab.genious_mind.gui_java.impl.network.java.lobby;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.impl.network.java.discovery.exception.SocketClosedException;
import com.gitlab.genious_mind.gui_java.impl.utils.NetworkUtils;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientMessageReceiver;
import lombok.extern.log4j.Log4j2;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

@Log4j2
public class TCPSocketManager implements AutoCloseable, Closeable {

    private final Socket socket;
    private final BufferedReader reader;
    private final PrintWriter writer;
    private final Packet remoteInfo;

    public TCPSocketManager(Socket socket) throws IOException {
        this.socket = socket;

        InputStream input = this.socket.getInputStream();
        this.reader = new BufferedReader(new InputStreamReader(input));

        OutputStream output = this.socket.getOutputStream();
        this.writer = new PrintWriter(output, true);

        this.remoteInfo = createRemoteInfo();
    }

    public void start(ClientMessageReceiver messageReceiver) {
        new Thread(() -> {
            try {
                do {
                    readMessage(messageReceiver);
                } while (!socket.isClosed());
            } catch (SocketException | SocketClosedException e) {
                log.debug("Disconnection...", e);
                messageReceiver.onDisconnection();
            }
        }).start();
    }

    private void readMessage(ClientMessageReceiver messageReceiver) throws SocketException {
        try {
            String message = reader.readLine();
            log.debug("Received message from {} : {}", remoteInfo, message);

            if (null == message) {
                throw new SocketClosedException();
            }

            messageReceiver.onMessageReceived(message);
        } catch (SocketException | SocketClosedException e) {
            throw e;
        } catch (Exception e) {
            log.warn("An exception occurred while reading from socket...", e);
        }
    }

    public void sendMessage(String message) {
        writer.println(message);
        log.debug("Sent message to {} : {}", remoteInfo, message);
    }

    @Override
    public void close() {
        try {
            writer.close();
            reader.close();
            socket.close();
        } catch (Exception e) {
            log.warn("An error occurred while disconnecting client!", e);
        }
    }

    public Packet getRemoteInfo() {
        return remoteInfo;
    }

    private Packet createRemoteInfo() {
        return NetworkUtils.createPacket((InetSocketAddress) socket.getRemoteSocketAddress());
    }

    public Packet getLocalInfo() {
        return NetworkUtils.createPacket((InetSocketAddress) socket.getLocalSocketAddress());
    }

}
