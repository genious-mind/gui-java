package com.gitlab.genious_mind.gui_java.impl.utils;

import com.gitlab.genious_mind.gui_java.impl.db.DatabaseService;
import com.gitlab.genious_mind.gui_java.impl.filesystem.JavaPropertiesService;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.configuration.ConfigurationSwing;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.direct.discovery.DirectConnectionSwing;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.discovery.ServerDiscovererSwing;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.game.GameSwing;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.language.LanguageSwing;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.lobby.ServerLobbySwing;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.menu.MainMenuSwing;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.menu.MultiPlayerMenuSwing;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.menu.SinglePlayerMenuSwing;
import com.gitlab.genious_mind.gui_java.impl.gui.swing.record.RecordSwing;
import com.gitlab.genious_mind.gui_java.impl.i18n.I18nService;
import com.gitlab.genious_mind.gui_java.impl.network.java.discovery.UDPClientJava;
import com.gitlab.genious_mind.gui_java.impl.network.java.discovery.UDPServerJava;
import com.gitlab.genious_mind.gui_java.impl.network.java.lobby.TCPClientJava;
import com.gitlab.genious_mind.gui_java.impl.network.java.lobby.TCPServerJava;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientReliableConnection;
import com.gitlab.genious_mind.gui_java.network.reliable.server.ServerReliableConnection;
import com.gitlab.genious_mind.gui_java.network.unreliable.UnreliableConnection;
import com.gitlab.genious_mind.gui_java.states.configuration.ConfigurationWindow;
import com.gitlab.genious_mind.gui_java.states.direct.DirectConnectionWindow;
import com.gitlab.genious_mind.gui_java.states.discoverer.ServerDiscovererWindow;
import com.gitlab.genious_mind.gui_java.states.game.client.GameWindow;
import com.gitlab.genious_mind.gui_java.states.language.LanguageSelectionWindow;
import com.gitlab.genious_mind.gui_java.states.lobby.client.ClientLobbyWindow;
import com.gitlab.genious_mind.gui_java.states.lobby.server.ServerLobbyWindow;
import com.gitlab.genious_mind.gui_java.states.menu.main.MainMenuWindow;
import com.gitlab.genious_mind.gui_java.states.menu.multiplayer.MultiPlayerMenuWindow;
import com.gitlab.genious_mind.gui_java.states.menu.singleplayer.SinglePlayerMenuWindow;
import com.gitlab.genious_mind.gui_java.states.record.RecordWindow;
import com.gitlab.genious_mind.gui_java.util.LanguageService;
import com.gitlab.genious_mind.gui_java.util.MatchService;
import com.gitlab.genious_mind.gui_java.util.PropertiesService;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import javax.inject.Named;
import java.util.Locale;

public class ClassModule extends AbstractModule {

    @Provides
    @Named("ClientUnreliableConnection")
    protected UnreliableConnection provideClientUnreliableConnection(UDPClientJava udpClientJava) {
        return udpClientJava;
    }

    @Provides
    @Named("ServerUnreliableConnection")
    protected UnreliableConnection provideServerUnreliableConnection(UDPServerJava udpServerJava) {
        return udpServerJava;
    }

    @Provides
    protected ServerLobbyWindow provideServerLobbyWindow(I18nService service) {
        return new ServerLobbySwing(service, false);
    }

    @Provides
    protected ClientLobbyWindow provideClientLobbyWindow(I18nService service) {
        return new ServerLobbySwing(service, true);
    }

    @Provides
    @Named("SinglePlayerGameWindow")
    protected GameWindow provideSinglePlayerGameWindow(I18nService service) {
        return new GameSwing(service, true);
    }

    @Provides
    @Named("MultiPlayerGameWindow")
    protected GameWindow provideMultiPlayerGameWindow(I18nService service) {
        return new GameSwing(service, false);
    }

    @Override
    protected void configure() {
        bind(Locale.class).toInstance(Locale.getDefault());
        bind(MainMenuWindow.class).to(MainMenuSwing.class);
        bind(PropertiesService.class).to(JavaPropertiesService.class);
        bind(RecordWindow.class).to(RecordSwing.class);
        bind(LanguageSelectionWindow.class).to(LanguageSwing.class);
        bind(LanguageService.class).to(I18nService.class);
        bind(SinglePlayerMenuWindow.class).to(SinglePlayerMenuSwing.class);
        bind(ConfigurationWindow.class).to(ConfigurationSwing.class);
        bind(MatchService.class).to(DatabaseService.class);
        bind(MultiPlayerMenuWindow.class).to(MultiPlayerMenuSwing.class);
        bind(ServerReliableConnection.class).to(TCPServerJava.class);
        bind(ClientReliableConnection.class).to(TCPClientJava.class);
        bind(DirectConnectionWindow.class).to(DirectConnectionSwing.class);
        bind(ServerDiscovererWindow.class).to(ServerDiscovererSwing.class);
    }

}
