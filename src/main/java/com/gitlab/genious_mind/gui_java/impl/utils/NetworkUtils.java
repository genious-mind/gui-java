package com.gitlab.genious_mind.gui_java.impl.utils;

import com.gitlab.genious_mind.gui_java.common.Packet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.nio.channels.UnresolvedAddressException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class NetworkUtils {

    private static final int REACHABLE_TIMEOUT = 5000;
    public static final String CHECK_IP_URL = "http://checkip.amazonaws.com/";

    private NetworkUtils() {
        throw new UnsupportedOperationException();
    }

    public static List<String> getAvailableAddressesFromAllNetwork() {
        return getAllNetworkInterfaces().filter(NetworkUtils::isNetworkInterfaceValid)
                .map(NetworkInterface::getInterfaceAddresses).flatMap(Collection::stream)
                .filter(interfaceAddress -> interfaceAddress.getBroadcast() != null)
                .map(InterfaceAddress::getAddress).filter(NetworkUtils::isReachable)
                .map(InetAddress::getHostAddress).collect(Collectors.toList());
    }

    private static Stream<NetworkInterface> getAllNetworkInterfaces() {
        try {
            Iterator<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces().asIterator();
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(networks, Spliterator.ORDERED), false);
        } catch (SocketException e) {
            return Stream.empty();
        }
    }

    private static boolean isNetworkInterfaceValid(NetworkInterface networkInterface) {
        try {
            return networkInterface != null && networkInterface.isUp() && !networkInterface.isLoopback();
        } catch (SocketException e) {
            return false;
        }
    }

    public static String getLanAddress(String clientAddress) throws UnknownHostException, SocketException {
        InetAddress inetAddress = InetAddress.getByName(clientAddress);
        NetworkInterface networkInterface = NetworkInterface.getByInetAddress(inetAddress);

        List<String> addresses = networkInterface.getInterfaceAddresses().stream()
                .map(InterfaceAddress::getAddress).filter(NetworkUtils::isReachable)
                .filter(Inet4Address.class::isInstance).map(InetAddress::getHostAddress)
                .collect(Collectors.toList());

        if (1 == addresses.size()) {
            return addresses.get(0);
        }

        throw new UnresolvedAddressException();
    }

    private static boolean isReachable(InetAddress inetAddress) {
        try {
            return inetAddress.isReachable(REACHABLE_TIMEOUT);
        } catch (IOException e) {
            return false;
        }
    }

    public static String getInternetAddress() throws IOException {
        URL url = new URL(CHECK_IP_URL);
        try (Scanner scanner = new Scanner(new InputStreamReader(url.openStream()))) {
            String address = scanner.nextLine();
            return InetAddress.getByName(address).getHostAddress();
        }
    }

    public static Packet createPacket(InetSocketAddress address) {
        return new Packet(address.getAddress().getHostAddress(), address.getPort());
    }

}
