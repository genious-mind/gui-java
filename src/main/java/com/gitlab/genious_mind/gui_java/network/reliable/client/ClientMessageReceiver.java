package com.gitlab.genious_mind.gui_java.network.reliable.client;

public interface ClientMessageReceiver {

    void onDisconnection();

    void onMessageReceived(String message);

}
