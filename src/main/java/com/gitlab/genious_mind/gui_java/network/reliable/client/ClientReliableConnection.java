package com.gitlab.genious_mind.gui_java.network.reliable.client;

import com.gitlab.genious_mind.gui_java.common.Packet;

import java.io.Closeable;
import java.io.IOException;

public interface ClientReliableConnection extends Closeable, AutoCloseable {

    void setMessageReceiver(ClientMessageReceiver messageReceiver);

    void start(Packet packet) throws IOException;

    void sendMessage(String message);

    void close();

    Packet getLocalInfo();

}
