package com.gitlab.genious_mind.gui_java.network.reliable.server;

public interface ServerMessageReceiver {

    void onDisconnection(int clientIndex);

    void onMessageReceived(int clientIndex, String message);

    void onConnection(int clientIndex);

}
