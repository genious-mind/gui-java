package com.gitlab.genious_mind.gui_java.network.reliable.server;

import com.gitlab.genious_mind.gui_java.common.Packet;

import java.io.Closeable;
import java.io.IOException;

public interface ServerReliableConnection extends Closeable, AutoCloseable {

    void start() throws IOException;

    Packet getClientInfo(int clientIndex);

    int getPort();

    void sendMessage(String message);

    void sendMessage(String message, int clientIndex);

    void setMessageReceiver(ServerMessageReceiver serverMessageReceiver);

    void close();

}
