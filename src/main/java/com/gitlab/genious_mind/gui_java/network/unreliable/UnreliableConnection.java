package com.gitlab.genious_mind.gui_java.network.unreliable;

import com.gitlab.genious_mind.gui_java.common.Packet;

import java.io.IOException;

public interface UnreliableConnection {

  boolean createSocket();

  Packet receiveMessage() throws IOException;

  void sendMessage(Packet packet) throws IOException;

  boolean closeSocket();

  boolean isSocketClosed();

}
