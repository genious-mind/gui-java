package com.gitlab.genious_mind.gui_java.states._continue;

import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.util.MatchService;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;
import com.gitlab.genious_mind.gui_java.util.core.MastermindAttempt;

import javax.inject.Inject;
import java.util.List;

public class Continue implements State {

    private final MatchService matchService;

    @Inject
    public Continue(MatchService matchService) {
        this.matchService = matchService;
    }

    @Override
    public void start(Context context, Object... args) {
        Mastermind match = matchService.loadMastermind();
        context.changeState(StateEnum.SINGLE_PLAYER, match);
    }

    @Override
    public void stop() {

    }
}
