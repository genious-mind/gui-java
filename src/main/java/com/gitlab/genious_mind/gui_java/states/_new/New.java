package com.gitlab.genious_mind.gui_java.states._new;

import com.gitlab.genious_mind.gui_java.StateManager;
import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.util.MatchService;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;

import javax.inject.Inject;

public class New implements State {

    private final MatchService matchService;

    @Inject
    public New(MatchService matchService) {
        this.matchService = matchService;
    }

    @Override
    public void start(Context context, Object... args) {
        MastermindConfig config = StateManager.getObjectFromArgs(args, 0, MastermindConfig.class);
        Mastermind game = new Mastermind(config);

        this.matchService.closeOpenMatch();
        this.matchService.insertNewMatch(game);

        context.changeState(StateEnum.SINGLE_PLAYER, game);
    }

    @Override
    public void stop() {

    }

}
