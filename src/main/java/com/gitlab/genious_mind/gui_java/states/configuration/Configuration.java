package com.gitlab.genious_mind.gui_java.states.configuration;

import com.gitlab.genious_mind.gui_java.StateManager;
import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.inject.Named;

import java.util.Properties;

import static com.gitlab.genious_mind.gui_java.util.core.MastermindConfig.*;

@Log4j2
public class Configuration implements State, ConfigurationEventHandler {

    private static final String CONFIGURATION_MAX_DIGIT = "configuration.max.digit";
    private static final String CONFIGURATION_NUMBER_LENGTH = "configuration.number.length";
    private final ConfigurationWindow window;
    private StateEnum nextState;
    private Context context;
    private MastermindConfig config;

    @Inject
    public Configuration(ConfigurationWindow window,
                         @Named(CONFIGURATION_MAX_DIGIT) int maxDigit,
                         @Named(CONFIGURATION_NUMBER_LENGTH) int numberLength) {
        this.window = window;
        this.config = new MastermindConfig(maxDigit, numberLength);
    }

    @Override
    public void start(Context context, Object... args) {
        this.nextState = StateManager.getObjectFromArgs(args, 0, StateEnum.class);
        this.context = context;

        window.setEventHandler(this);
        window.open(MIN_DIGIT, MAX_DIGIT, MIN_NUMBER_LENGTH, MAX_NUMBER_LENGTH, config);
    }

    @Override
    public void stop() {
        window.close();
    }

    @Override
    public MastermindConfig onMaxDigitChange(int maxDigit) {
        if (MastermindConfig.isValidMaxDigit(maxDigit)) {
            try {
                config = new MastermindConfig(maxDigit, config.getNumberLength());
            } catch (NumberFormatException e) {
                config = new MastermindConfig(maxDigit, maxDigit - 1);
            }
        }
        return config;
    }

    @Override
    public MastermindConfig onNumberLengthChange(int numberLength) {
        if (MastermindConfig.isValidNumberLength(numberLength)) {
            try {
                config = new MastermindConfig(config.getMaxDigit(), numberLength);
            } catch (NumberFormatException e) {
                config = new MastermindConfig(numberLength + 1, numberLength);
            }
        }
        return config;
    }

    @Override
    public void onStart() {
        Properties props = new Properties() {{
            put(CONFIGURATION_MAX_DIGIT, Integer.toString(config.getMaxDigit()));
            put(CONFIGURATION_NUMBER_LENGTH, Integer.toString(config.getNumberLength()));
        }};
        context.updateUserProperties(props);
        context.changeState(nextState, config);
    }

    @Override
    public void onCancel() {
        context.changeState(StateEnum.SINGLE_PLAYER_MENU);
    }

}
