package com.gitlab.genious_mind.gui_java.states.configuration;

import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;

public interface ConfigurationEventHandler {

    MastermindConfig onMaxDigitChange(int maxDigit);

    MastermindConfig onNumberLengthChange(int numberLength);

    void onStart();

    void onCancel();

}
