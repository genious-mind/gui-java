package com.gitlab.genious_mind.gui_java.states.configuration;

import com.gitlab.genious_mind.gui_java.common.Window;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;

public interface ConfigurationWindow extends Window {

    void open(int minMaxDigit, int maxMaxDigit, int minNumberLength, int maxNumberLength, MastermindConfig config);

    void setEventHandler(ConfigurationEventHandler eventHandler);

}
