package com.gitlab.genious_mind.gui_java.states.direct;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import java.net.InetAddress;
import java.util.Optional;

@Log4j2
public class DirectConnection implements State, DirectConnectionEventHandler {

    private static final int MAX_PORT_NUMBER = 0xFFFF;
    private final DirectConnectionWindow directConnectionWindow;
    private Context context;

    @Inject
    public DirectConnection(DirectConnectionWindow directConnectionWindow) {
        this.directConnectionWindow = directConnectionWindow;
    }

    @Override
    public void start(Context context, Object... object) {
        this.context = context;

        directConnectionWindow.setEventHandler(this);
        directConnectionWindow.open();
    }

    @Override
    public void onConnect(String address, String port) {
        if (!isValidAddress(address)) {
            this.directConnectionWindow.showInvalidAddressError(address);
        } else if (!isValidPort(port)) {
            this.directConnectionWindow.showInvalidPortNumberError(port);
        } else {
            context.changeState(StateEnum.CLIENT_LOBBY, new Packet(address, Integer.parseInt(port)));
        }
    }

    @Override
    public void onCancel() {
        context.changeState(StateEnum.MULTI_PLAYER_MENU);
    }

    @Override
    public void stop() {
        directConnectionWindow.close();
    }

    public static boolean isValidAddress(String address) {
        return Optional.ofNullable(address).filter(DirectConnection::isValidIpAddress).isPresent();
    }

    private static boolean isValidIpAddress(String address) {
        try {
            return (null != InetAddress.getByName(address));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isValidPort(String portString) {
        try {
            int port = Integer.parseInt(portString);
            return (1 <= port && port <= MAX_PORT_NUMBER);
        } catch (Exception e) {
            return false;
        }
    }

}
