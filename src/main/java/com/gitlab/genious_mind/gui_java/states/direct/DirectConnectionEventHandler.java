package com.gitlab.genious_mind.gui_java.states.direct;

public interface DirectConnectionEventHandler {

    void onConnect(String address, String port);

    void onCancel();

}
