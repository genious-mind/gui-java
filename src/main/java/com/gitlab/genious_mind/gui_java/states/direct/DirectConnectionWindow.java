package com.gitlab.genious_mind.gui_java.states.direct;

import com.gitlab.genious_mind.gui_java.common.Window;

public interface DirectConnectionWindow extends Window {

    void open();

    void setEventHandler(DirectConnectionEventHandler eventHandler);

    void showInvalidPortNumberError(String port);

    void showInvalidAddressError(String address);

}
