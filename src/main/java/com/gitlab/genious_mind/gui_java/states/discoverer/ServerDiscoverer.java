package com.gitlab.genious_mind.gui_java.states.discoverer;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Log4j2
public class ServerDiscoverer implements State, ServerDiscovererEventHandler {

    private final ServerDiscovererConnection serverDiscovererConnection;
    private final ServerDiscovererWindow serverDiscovererWindow;
    private final Set<Packet> serverInfoList;
    private Context context;

    @Inject
    public ServerDiscoverer(ServerDiscovererConnection serverDiscovererConnection,
                            ServerDiscovererWindow serverDiscovererWindow) {
        this.serverDiscovererConnection = serverDiscovererConnection;
        this.serverDiscovererWindow = serverDiscovererWindow;
        this.serverInfoList = new HashSet<>();
    }

    @Override
    public void start(Context context, Object... object) {
        this.context = context;

        serverDiscovererWindow.setEventHandler(this);
        serverDiscovererWindow.open();

        serverDiscovererConnection.onUpdateFunction(this::updateServerList);
        new Thread(serverDiscovererConnection).start();
    }

    private void updateServerList(Packet serverInfo) {
        Optional<Packet> oldServerInfo = serverInfoList.stream().filter(packet -> packet.isSameSender(serverInfo)).findAny();
        oldServerInfo.ifPresent(serverInfoList::remove);
        serverInfoList.add(serverInfo);

        serverDiscovererWindow.updateServerList(serverInfoList);
    }

    @Override
    public void onConnect(Packet serverInfo) {
        if (null != serverInfo) {
            context.changeState(StateEnum.CLIENT_LOBBY, serverInfo);
        }
    }

    @Override
    public void onCancel() {
        context.changeState(StateEnum.MULTI_PLAYER_MENU);
    }

    @Override
    public void stop() {
        serverDiscovererWindow.close();
        serverDiscovererConnection.close();
    }

}
