package com.gitlab.genious_mind.gui_java.states.discoverer;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.network.unreliable.UnreliableConnection;
import com.gitlab.genious_mind.gui_java.impl.utils.NetworkUtils;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.inject.Named;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Log4j2
public class ServerDiscovererConnection implements Runnable {

    private final UnreliableConnection unreliableConnection;
    private final int port;
    private Consumer<Packet> updateServerList;

    @Inject
    public ServerDiscovererConnection(@Named("ClientUnreliableConnection") UnreliableConnection unreliableConnection,
                                      @Named("connection.unreliable.server.port") int port) {
        this.unreliableConnection = unreliableConnection;
        this.port = port;
    }

    public void onUpdateFunction(Consumer<Packet> updateServerList) {
        this.updateServerList = updateServerList;
    }

    @Override
    public void run() {
        unreliableConnection.createSocket();

        do {
            try {
                List<Packet> servers = NetworkUtils.getAvailableAddressesFromAllNetwork().stream()
                        .map(address -> new Packet(address, port)).collect(Collectors.toList());

                for (Packet packet : servers) {
                    log.debug("Message sent to the server: {}", packet);
                    unreliableConnection.sendMessage(packet);
                }

                messageReceiver();
            } catch (Exception e) {
                log.error("Error while sending a message to the server", e);
            }
        } while (!unreliableConnection.isSocketClosed());
    }

    private void messageReceiver() {
        do {
            try {
                Packet packet = unreliableConnection.receiveMessage();
                log.debug("Message received from the server: {}", packet);

                //TODO: make get & set atomic
                Packet serverInfo = Packet.fromJson(packet.getMessage());

                updateServerList.accept(serverInfo);
            } catch (SocketTimeoutException e) {
                return;
            } catch (Exception e) {
                log.error("Error while receiving a message from the server", e);
            }
        } while (!unreliableConnection.isSocketClosed());
    }

    public void close() {
        unreliableConnection.closeSocket();
    }

}
