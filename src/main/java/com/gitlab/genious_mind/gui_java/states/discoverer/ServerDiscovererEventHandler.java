package com.gitlab.genious_mind.gui_java.states.discoverer;

import com.gitlab.genious_mind.gui_java.common.Packet;

public interface ServerDiscovererEventHandler {

    void onConnect(Packet serverInfo);

    void onCancel();

}
