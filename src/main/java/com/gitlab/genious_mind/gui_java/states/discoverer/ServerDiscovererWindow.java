package com.gitlab.genious_mind.gui_java.states.discoverer;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.common.Window;

import java.util.Set;

public interface ServerDiscovererWindow extends Window {

    void open();

    void setEventHandler(ServerDiscovererEventHandler eventHandler);

    void updateServerList(Set<Packet> servers);

}
