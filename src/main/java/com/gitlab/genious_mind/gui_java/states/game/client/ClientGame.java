package com.gitlab.genious_mind.gui_java.states.game.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gitlab.genious_mind.gui_java.StateManager;
import com.gitlab.genious_mind.gui_java.common.message.Message;
import com.gitlab.genious_mind.gui_java.common.message.MessageType;
import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientReliableConnection;
import com.gitlab.genious_mind.gui_java.states.single.SinglePlayerEventHandler;
import com.gitlab.genious_mind.gui_java.util.core.MastermindAttempt;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.*;

@Log4j2
public class ClientGame implements State, ClientMessageReceiver, GameEventHandler {

    private final GameWindow gameWindow;
    private final Deque<Integer> numberToDisplay;
    private final List<MastermindAttempt> attemptList;
    private final SinglePlayerEventHandler eventHandler;
    private Context context;
    private ClientReliableConnection clientConnection;
    private MastermindConfig mastermindConfig;

    @Inject
    public ClientGame(@Named("MultiPlayerGameWindow") GameWindow gameWindow) {
        this(gameWindow, new SinglePlayerEventHandler() {
            @Override
            public void onExit() {
            }

            @Override
            public void onWin() {
            }
        }, new ArrayList<>());
    }

    public ClientGame(GameWindow gameWindow, SinglePlayerEventHandler eventHandler, List<MastermindAttempt> attempts) {
        this.gameWindow = gameWindow;
        this.eventHandler = eventHandler;
        this.attemptList = attempts;

        this.numberToDisplay = new ArrayDeque<>();
    }

    @Override
    public void start(Context context, Object... args) {
        this.context = context;

        this.mastermindConfig = getConfigFromArgs(args);

        this.clientConnection = getConnectionFromArgs(args);
        this.clientConnection.setMessageReceiver(this);

        this.gameWindow.setEventHandler(this);
        this.gameWindow.updateDisplay(this.numberToDisplay);
        this.gameWindow.updateAttemptList(this.attemptList);
        this.gameWindow.open(mastermindConfig);
    }

    private ClientReliableConnection getConnectionFromArgs(Object[] args) {
        return StateManager.getObjectFromArgs(args, 0, ClientReliableConnection.class);
    }

    private MastermindConfig getConfigFromArgs(Object[] args) {
        return StateManager.getObjectFromArgs(args, 1, MastermindConfig.class);
    }

    @Override
    public void onDisconnection() {
        onMessageReceived(new Message<>(MessageType.DISCONNECTED).toJson());
    }

    @Override
    public void onMessageReceived(String json) {
        MessageType messageType = Message.getTypeFromJson(json);
        switch (messageType) {
            case ATTEMPT_RESPONSE:
                this.attemptList.add(Message.getMessageFromJson(new TypeReference<>() {
                }, json));
                this.gameWindow.updateAttemptList(attemptList);
                break;
            case LOSER:
                MastermindAttempt attempt = Message.getMessageFromJson(new TypeReference<>() {
                }, json);
                this.gameWindow.showDefeatMessage(attempt.getAttempt());
                this.gameWindow.disableInterface();
                this.clientConnection.close();
                break;
            case WINNER:
                Long score = Message.getMessageFromJson(new TypeReference<>() {
                }, json);
                this.gameWindow.showVictoryMessage(score);
                this.gameWindow.disableInterface();
                this.clientConnection.close();
                this.eventHandler.onWin();
                break;
            case DISCONNECTED:
                this.gameWindow.showServerDisconnectionError();
                this.context.changeState(StateEnum.MAIN_MENU);
                break;
            case PLAYER_DISCONNECTED:
                String playerName = Message.getMessageFromJson(new TypeReference<>() {
                }, json);
                this.gameWindow.showPlayerDisconnectedError(playerName);
                break;
            case TIME:
                long time = Message.getMessageFromJson(new TypeReference<>() {
                }, json);
                this.gameWindow.updateTime(time);
                break;
            default:
                log.debug("Unexpected message: {}", json);
        }
    }

    @Override
    public void stop() {
        this.gameWindow.close();
        this.clientConnection.close();
    }

    @Override
    public void onPressNumber(int number) {
        this.numberToDisplay.addLast(number);

        if (!this.mastermindConfig.isNumberValid(toArray(numberToDisplay))) {
            this.numberToDisplay.removeLast();
        }

        this.gameWindow.updateDisplay(numberToDisplay);
    }

    @Override
    public void onDelete() {
        if (!this.numberToDisplay.isEmpty()) {
            this.numberToDisplay.removeLast();
        }

        this.gameWindow.updateDisplay(numberToDisplay);
    }

    @Override
    public void onSend() {
        if (this.mastermindConfig.isNumberValidAndComplete(toArray(numberToDisplay))) {
            this.clientConnection.sendMessage(new Message<>(MessageType.ATTEMPT_REQUEST, numberToDisplay).toJson());
            this.numberToDisplay.clear();
            this.gameWindow.updateDisplay(numberToDisplay);
        }
    }

    @Override
    public void onExit() {
        boolean isExitConfirmed = this.gameWindow.showExitConfirmation();
        if (isExitConfirmed) {
            this.eventHandler.onExit();
            this.context.changeState(StateEnum.MAIN_MENU);
        }
    }

    private static int[] toArray(Collection<Integer> stack) {
        return stack.stream().mapToInt(Integer::intValue).toArray();
    }

}