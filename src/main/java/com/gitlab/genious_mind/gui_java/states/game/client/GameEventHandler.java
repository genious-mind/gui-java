package com.gitlab.genious_mind.gui_java.states.game.client;

public interface GameEventHandler {

    void onPressNumber(int number);

    void onDelete();

    void onSend();

    void onExit();

}
