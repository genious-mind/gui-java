package com.gitlab.genious_mind.gui_java.states.game.client;

import com.gitlab.genious_mind.gui_java.common.Window;
import com.gitlab.genious_mind.gui_java.util.core.MastermindAttempt;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;

import java.util.Collection;
import java.util.List;

public interface GameWindow extends Window {

    void open(MastermindConfig config);

    void setEventHandler(GameEventHandler eventHandler);

    void updateDisplay(Collection<Integer> string);

    void updateAttemptList(List<MastermindAttempt> attemptList);

    void disableInterface();

    boolean showExitConfirmation();

    void showVictoryMessage(Long score);

    void showDefeatMessage(int[] number);

    void showPlayerDisconnectedError(String playerName);

    void updateTime(long time);

}
