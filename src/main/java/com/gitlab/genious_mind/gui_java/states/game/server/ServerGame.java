package com.gitlab.genious_mind.gui_java.states.game.server;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.common.message.Message;
import com.gitlab.genious_mind.gui_java.common.message.MessageType;
import com.gitlab.genious_mind.gui_java.network.reliable.server.ServerMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.server.ServerReliableConnection;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;
import com.gitlab.genious_mind.gui_java.util.core.MastermindAttempt;
import lombok.extern.log4j.Log4j2;

import java.io.Closeable;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

@Log4j2
public class ServerGame implements ServerMessageReceiver, Closeable, AutoCloseable {

    private static final long INTERVAL_TIME = 1000;

    private final ServerReliableConnection serverConnection;
    private final Map<Integer, Packet> clients;
    private final Mastermind mastermind;
    private final Timer timer;
    private final int masterClientId;

    public ServerGame(ServerReliableConnection serverConnection, Map<Integer, Packet> clients, int masterClientId, Mastermind mastermind) {
        this.clients = clients;
        this.masterClientId = masterClientId;
        this.mastermind = mastermind;
        this.serverConnection = serverConnection;
        this.serverConnection.setMessageReceiver(this);

        this.timer = new Timer();
    }

    public void start() {
        serverConnection.sendMessage(new Message<>(MessageType.START, mastermind.getConfig()).toJson());
        timer.scheduleAtFixedRate(getTimerTask(), 0, INTERVAL_TIME);
    }

    private TimerTask getTimerTask() {
        return new TimerTask() {
            @Override
            public void run() {
                mastermind.updateTime();

                String message = new Message<>(MessageType.TIME, mastermind.getElapsedSeconds()).toJson();
                serverConnection.sendMessage(message);
            }
        };
    }

    @Override
    public void onMessageReceived(int clientIndex, String json) {
        MessageType type = Message.getTypeFromJson(json);

        if (MessageType.ATTEMPT_REQUEST == type) {
            int[] attemptFromClient = Message.getMessageFromJson(new TypeReference<>() {
            }, json);
            MastermindAttempt attempt = mastermind.evaluateAttempt(attemptFromClient);

            String attemptMessage = new Message<>(MessageType.ATTEMPT_RESPONSE, attempt).toJson();
            this.serverConnection.sendMessage(attemptMessage, clientIndex);

            if (mastermind.isGameOver()) {
                timer.cancel();

                sendMessagesToLosers(clientIndex, attempt);

                String scoreMessage = new Message<>(MessageType.WINNER, mastermind.getScore()).toJson();
                this.serverConnection.sendMessage(scoreMessage, clientIndex);
            }
        }
    }

    private void sendMessagesToLosers(int clientIndex, MastermindAttempt attempt) {
        String message = new Message<>(MessageType.LOSER, attempt).toJson();
        clients.keySet().stream()
                .filter(index -> index != clientIndex)
                .forEach(index -> serverConnection.sendMessage(message, index));
    }

    public Mastermind getMastermind() {
        return mastermind;
    }

    @Override
    public void onConnection(int clientIndex) {
        serverConnection.sendMessage(new Message<>(MessageType.GAME_ALREADY_STARTED).toJson(), clientIndex);
    }

    @Override
    public void onDisconnection(int clientIndex) {
        if (clients.containsKey(clientIndex)) {
            Packet client = clients.remove(clientIndex);

            serverConnection.sendMessage(new Message<>(MessageType.PLAYER_DISCONNECTED, client.getMessage()).toJson());

            if (clients.isEmpty() || masterClientId == clientIndex) {
                close();
            }
        }
    }

    @Override
    public void close() {
        serverConnection.close();
        timer.cancel();
        log.debug("Shutdown completed!");
    }

}