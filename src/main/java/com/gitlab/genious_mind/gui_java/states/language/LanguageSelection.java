package com.gitlab.genious_mind.gui_java.states.language;

import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.util.LanguageService;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Locale;
import java.util.Properties;

public class LanguageSelection implements State, LanguageSelectionEventHandler {

    public static final String LANGUAGE_PROPERTY = "language.default";
    private final LanguageSelectionWindow languageSelectionWindow;
    private final LanguageService languageService;
    private Context context;

    @Inject
    public LanguageSelection(LanguageSelectionWindow languageSelectionWindow,
                             LanguageService languageService) {
        this.languageSelectionWindow = languageSelectionWindow;
        this.languageService = languageService;
    }

    @Override
    public void start(Context context, Object... object) {
        this.context = context;

        Collection<Locale> availableLanguages = languageService.getAvailableLanguages();
        Locale currentLocale = languageService.getCurrentLanguage();

        languageSelectionWindow.setEventHandler(this);
        languageSelectionWindow.open(availableLanguages, currentLocale);
    }

    @Override
    public void onSave(Locale currentLocale) {
        languageService.setCurrentLanguage(currentLocale);
        Properties props = new Properties() {{
            put(LANGUAGE_PROPERTY, currentLocale.getLanguage());
        }};
        context.updateUserProperties(props);
        context.changeState(StateEnum.MAIN_MENU);
    }

    @Override
    public void onCancel() {
        context.changeState(StateEnum.MAIN_MENU);
    }

    @Override
    public void stop() {
        languageSelectionWindow.close();
    }

}
