package com.gitlab.genious_mind.gui_java.states.language;

import java.util.Locale;

public interface LanguageSelectionEventHandler {

    void onSave(Locale currentLocale);

    void onCancel();

}
