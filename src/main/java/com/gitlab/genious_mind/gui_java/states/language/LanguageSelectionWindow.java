package com.gitlab.genious_mind.gui_java.states.language;

import com.gitlab.genious_mind.gui_java.common.Window;

import java.util.Collection;
import java.util.Locale;

public interface LanguageSelectionWindow extends Window {

    void open(Collection<Locale> availableLanguages, Locale currentLocale);

    void setEventHandler(LanguageSelectionEventHandler eventHandler);

}
