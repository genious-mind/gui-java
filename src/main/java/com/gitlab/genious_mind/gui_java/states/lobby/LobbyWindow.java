package com.gitlab.genious_mind.gui_java.states.lobby;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.common.Window;

import java.util.Collection;

public interface LobbyWindow extends Window {

  void open(Packet serverInfo);

  void updateClients(Collection<Packet> clients);

}
