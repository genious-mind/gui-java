package com.gitlab.genious_mind.gui_java.states.lobby.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gitlab.genious_mind.gui_java.StateManager;
import com.gitlab.genious_mind.gui_java.common.message.Message;
import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.common.message.MessageType;
import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientReliableConnection;
import com.gitlab.genious_mind.gui_java.states.lobby.server.ServerLobbyEventHandler;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import java.util.Collection;

@Log4j2
public class ClientLobby implements State, ClientMessageReceiver, ServerLobbyEventHandler {

    private final ClientReliableConnection clientReliableConnection;
    private final ClientLobbyWindow clientLobbyWindow;
    private Context context;

    @Inject
    public ClientLobby(ClientReliableConnection clientReliableConnection, ClientLobbyWindow clientLobbyWindow) {
        this.clientReliableConnection = clientReliableConnection;
        this.clientLobbyWindow = clientLobbyWindow;
    }

    @Override
    public void start(Context context, Object... args) {
        this.context = context;

        Packet serverInfo = getPacketFromArgs(args);

        clientLobbyWindow.setClientEventHandler(this);
        clientLobbyWindow.open(serverInfo);

        try {
            clientReliableConnection.setMessageReceiver(this);
            clientReliableConnection.start(serverInfo);
        } catch (Exception e) {
            log.error("An error occurred while connecting to {}", serverInfo, e);
            clientLobbyWindow.showConnectionError(serverInfo.getAddress(), serverInfo.getPort());
            onCancel();
        }
    }

    private static Packet getPacketFromArgs(Object[] args) {
        return StateManager.getObjectFromArgs(args, 0, Packet.class);
    }

    @Override
    public void onDisconnection() {
        onMessageReceived(new Message<>(MessageType.DISCONNECTED).toJson());
    }

    @Override
    public void onMessageReceived(String json) {
        MessageType messageType = Message.getTypeFromJson(json);
        switch (messageType) {
            case UPDATE_SERVER_LIST:
                Collection<Packet> serverList = Message.getMessageFromJson(new TypeReference<>() {
                }, json);
                clientLobbyWindow.updateClients(serverList);
                break;
            case START:
                MastermindConfig config = Message.getMessageFromJson(new TypeReference<>() {
                }, json);
                this.context.changeState(StateEnum.CLIENT_GAME, this.clientReliableConnection, config);
                break;
            case DISCONNECTED:
                clientLobbyWindow.showServerDisconnectionError();
                onCancel();
                break;
            case MAX_PLAYER_REACHED:
                clientLobbyWindow.showMaxPlayerReachedError();
                onCancel();
                break;
            case GAME_ALREADY_STARTED:
                clientLobbyWindow.showGameAlreadyStarted();
                onCancel();
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override
    public void onStart() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onCancel() {
        this.clientReliableConnection.close();
        this.context.changeState(StateEnum.MULTI_PLAYER_MENU);
    }

    @Override
    public void stop() {
        this.clientLobbyWindow.close();
    }

}
