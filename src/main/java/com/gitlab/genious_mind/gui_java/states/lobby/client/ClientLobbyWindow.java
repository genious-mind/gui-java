package com.gitlab.genious_mind.gui_java.states.lobby.client;

import com.gitlab.genious_mind.gui_java.states.lobby.LobbyWindow;

public interface ClientLobbyWindow extends LobbyWindow {

  void setClientEventHandler(ClientLobbyEventHandler clientEventHandler);

  void showConnectionError(String address, int port);

  void showMaxPlayerReachedError();

  void showGameAlreadyStarted();

}
