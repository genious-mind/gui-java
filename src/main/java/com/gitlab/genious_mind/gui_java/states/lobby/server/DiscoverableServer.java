package com.gitlab.genious_mind.gui_java.states.lobby.server;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.impl.utils.NetworkUtils;
import com.gitlab.genious_mind.gui_java.network.unreliable.UnreliableConnection;
import lombok.extern.log4j.Log4j2;

import java.net.SocketException;
import java.net.UnknownHostException;

@Log4j2
public class DiscoverableServer implements Runnable {

  private final UnreliableConnection udpServer;
  private final int serverPort;
  private final String serverName;

  public DiscoverableServer(UnreliableConnection unreliableConnection, int serverPort, String serverName) {
    this.udpServer = unreliableConnection;
    this.serverPort = serverPort;
    this.serverName = serverName;
  }

  public void run() {
    udpServer.createSocket();
    do {
      try {
        Packet clientPacket = udpServer.receiveMessage();
        log.debug("Message received from a client: {}", clientPacket);

        udpServer.sendMessage(createServerInfoPacket(clientPacket, serverPort, serverName));
      } catch (Exception e) {
        log.error("Error while receiving/sending a message from/to a client", e);
      }
    } while (!udpServer.isSocketClosed());

    udpServer.closeSocket();
  }

  private static Packet createServerInfoPacket(Packet clientPacket, int serverPort, String serverName) throws SocketException, UnknownHostException {
    String serverAddress = NetworkUtils.getLanAddress(clientPacket.getAddress());
    String serverInfo = Packet.toJson(new Packet(serverAddress, serverPort, serverName));
    return new Packet(clientPacket.getAddress(), clientPacket.getPort(), serverInfo);
  }

}
