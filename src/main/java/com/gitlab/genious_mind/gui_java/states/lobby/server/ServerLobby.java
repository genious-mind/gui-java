package com.gitlab.genious_mind.gui_java.states.lobby.server;

import com.gitlab.genious_mind.gui_java.StateManager;
import com.gitlab.genious_mind.gui_java.common.message.Message;
import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.common.message.MessageType;
import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.impl.utils.NetworkUtils;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientReliableConnection;
import com.gitlab.genious_mind.gui_java.network.reliable.server.ServerMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.server.ServerReliableConnection;
import com.gitlab.genious_mind.gui_java.network.unreliable.UnreliableConnection;
import com.gitlab.genious_mind.gui_java.states.game.server.ServerGame;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;
import com.gitlab.genious_mind.gui_java.util.core.MastermindConfig;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.inject.Named;
import java.nio.channels.UnresolvedAddressException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

@Log4j2
public class ServerLobby implements State, ServerLobbyEventHandler, ServerMessageReceiver {

    private static final String SERVER_ADDRESS = "localhost";

    private final ServerReliableConnection serverConnection;
    private final UnreliableConnection unreliableConnection;
    private final ClientReliableConnection clientConnection;
    private final ServerLobbyWindow lobbyWindow;
    private final HashMap<Integer, Packet> clients;
    private final Long maxClientNumber;
    private MastermindConfig config;
    private Context context;
    private int masterClientId;

    @Inject
    public ServerLobby(@Named("ServerUnreliableConnection") UnreliableConnection unreliableConnection,
                       ServerReliableConnection serverConnection,
                       ClientReliableConnection clientConnection,
                       ServerLobbyWindow lobbyWindow) {
        this.unreliableConnection = unreliableConnection;
        this.serverConnection = serverConnection;
        this.clientConnection = clientConnection;
        this.lobbyWindow = lobbyWindow;
        this.clients = new HashMap<>();
        this.maxClientNumber = 4L;
    }

    @Override
    public void start(Context context, Object... args) {
        this.context = context;
        this.config = StateManager.getObjectFromArgs(args, 0, MastermindConfig.class);

        try {
            serverConnection.setMessageReceiver(this);
            serverConnection.start();

            int serverPort = serverConnection.getPort();
            log.info("Server started on port {}", serverPort);

            String serverName = lobbyWindow.askServerName();

            lobbyWindow.setEventHandler(this);
            lobbyWindow.open(getServerInfo(serverPort, serverName));

            startClientConnection(SERVER_ADDRESS, serverPort);

            startDiscoverableServer(serverPort, serverName);
        } catch (Exception e) {
            log.error("An error occurred while starting the server!", e);
            lobbyWindow.showConnectionError();
        }
    }

    private void startClientConnection(String serverAddress, int serverPort) throws IOException {
        clientConnection.start(new Packet(serverAddress, serverPort));
        clientConnection.setMessageReceiver(new ClientMessageReceiver() {
            @Override
            public void onDisconnection() {
                clientConnection.close();
            }

            @Override
            public void onMessageReceived(String message) {
                log.debug("Received message : {}", message);
            }
        });
    }

    private static Packet getServerInfo(int serverPort, String serverName) {
        try {
            return new Packet(NetworkUtils.getInternetAddress(), serverPort, serverName);
        } catch (Exception e) {
            throw new UnresolvedAddressException();
        }
    }

    private void startDiscoverableServer(int serverPort, String serverName) {
        new Thread(new DiscoverableServer(unreliableConnection, serverPort, serverName)).start();
    }

    @Override
    public void onMessageReceived(int clientIndex, String message) {
        log.debug("Received message from client {}: {}", clientIndex, message);
    }

    @Override
    public void onConnection(int clientIndex) {
        if (isMaxClientNumberReached()) {
            serverConnection.sendMessage(new Message<>(MessageType.MAX_PLAYER_REACHED).toJson(), clientIndex);
        } else {
            if (isMasterClient(clientIndex)) {
                masterClientId = clientIndex;
            }
            clients.put(clientIndex, serverConnection.getClientInfo(clientIndex));
            updateClients();
        }
    }

    private boolean isMasterClient(int clientIndex) {
        return serverConnection.getClientInfo(clientIndex).isSameSender(clientConnection.getLocalInfo());
    }

    private boolean isMaxClientNumberReached() {
        return null != maxClientNumber && clients.size() >= maxClientNumber;
    }

    @Override
    public void onDisconnection(int clientIndex) {
        if (clients.containsKey(clientIndex)) {
            this.clients.remove(clientIndex);
            updateClients();
        }
    }

    private void updateClients() {
        Collection<Packet> clientList = this.clients.values();
        Message<Collection<Packet>> message = new Message<>(MessageType.UPDATE_SERVER_LIST, clientList);
        this.serverConnection.sendMessage(message.toJson());
        this.lobbyWindow.updateClients(clientList);
    }

    @Override
    public void onStart() {
        ServerGame gameServer = new ServerGame(serverConnection, clients, masterClientId, new Mastermind(config));
        gameServer.start();

        context.changeState(StateEnum.CLIENT_GAME, clientConnection, config);
    }

    @Override
    public void onCancel() {
        this.serverConnection.close();
        context.changeState(StateEnum.MULTI_PLAYER_MENU);
    }

    @Override
    public void stop() {
        this.unreliableConnection.closeSocket();
        this.lobbyWindow.close();
        log.debug("Shutdown completed!");
    }

}
