package com.gitlab.genious_mind.gui_java.states.lobby.server;

import com.gitlab.genious_mind.gui_java.states.lobby.client.ClientLobbyEventHandler;

public interface ServerLobbyEventHandler extends ClientLobbyEventHandler {

    void onStart();

}
