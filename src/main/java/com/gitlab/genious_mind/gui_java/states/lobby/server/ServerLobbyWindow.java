package com.gitlab.genious_mind.gui_java.states.lobby.server;

import com.gitlab.genious_mind.gui_java.states.lobby.LobbyWindow;

public interface ServerLobbyWindow extends LobbyWindow {

    void setEventHandler(ServerLobbyEventHandler eventHandler);

    String askServerName();

    void showConnectionError();

}
