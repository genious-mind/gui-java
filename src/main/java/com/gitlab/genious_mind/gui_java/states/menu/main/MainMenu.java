package com.gitlab.genious_mind.gui_java.states.menu.main;

import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;

import javax.inject.Inject;

public class MainMenu implements State, MainMenuEventHandler {

    private final MainMenuWindow mainMenuWindow;
    private Context context;

    @Inject
    public MainMenu(MainMenuWindow mainMenuWindow) {
        this.mainMenuWindow = mainMenuWindow;
    }

    @Override
    public void start(Context context, Object... object) {
        this.context = context;

        mainMenuWindow.setEventHandler(this);
        mainMenuWindow.open();
    }

    @Override
    public void onSinglePlayer() {
        context.changeState(StateEnum.SINGLE_PLAYER_MENU);
    }

    @Override
    public void onMultiPlayer() {
        context.changeState(StateEnum.MULTI_PLAYER_MENU);
    }

    @Override
    public void onLanguage() {
        context.changeState(StateEnum.LANGUAGE);
    }

    @Override
    public void onExit() {
        mainMenuWindow.close();
    }

    @Override
    public void stop() {
        mainMenuWindow.close();
    }

}
