package com.gitlab.genious_mind.gui_java.states.menu.main;

public interface MainMenuEventHandler {

    void onSinglePlayer();

    void onMultiPlayer();

    void onLanguage();

    void onExit();

}
