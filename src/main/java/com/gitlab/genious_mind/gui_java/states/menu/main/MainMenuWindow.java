package com.gitlab.genious_mind.gui_java.states.menu.main;

import com.gitlab.genious_mind.gui_java.common.Window;

public interface MainMenuWindow extends Window {

    void open();

    void setEventHandler(MainMenuEventHandler eventHandler);

}
