package com.gitlab.genious_mind.gui_java.states.menu.multiplayer;

import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;

import javax.inject.Inject;

public class MultiPlayerMenu implements State, MultiPlayerMenuEventHandler {

    private final MultiPlayerMenuWindow multiPlayerMenuWindow;
    private Context context;

    @Inject
    public MultiPlayerMenu(MultiPlayerMenuWindow multiPlayerMenuWindow) {
        this.multiPlayerMenuWindow = multiPlayerMenuWindow;
    }

    @Override
    public void start(Context context, Object... object) {
        this.context = context;

        multiPlayerMenuWindow.setEventHandler(this);
        multiPlayerMenuWindow.open();
    }

    @Override
    public void onDiscoverServer() {
        context.changeState(StateEnum.SERVER_DISCOVERER);
    }

    @Override
    public void onDirectConnection() {
        context.changeState(StateEnum.DIRECT_CONNECTION);
    }

    @Override
    public void onCreateLobby() {
        context.changeState(StateEnum.CONFIGURATION, StateEnum.SERVER_LOBBY);
    }

    @Override
    public void onGoBack() {
        context.changeState(StateEnum.MAIN_MENU);
    }

    @Override
    public void stop() {
        multiPlayerMenuWindow.close();
    }

}
