package com.gitlab.genious_mind.gui_java.states.menu.multiplayer;

public interface MultiPlayerMenuEventHandler {

    void onDiscoverServer();

    void onDirectConnection();

    void onCreateLobby();

    void onGoBack();

}
