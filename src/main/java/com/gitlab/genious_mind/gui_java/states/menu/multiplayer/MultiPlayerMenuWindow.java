package com.gitlab.genious_mind.gui_java.states.menu.multiplayer;

import com.gitlab.genious_mind.gui_java.common.Window;

public interface MultiPlayerMenuWindow extends Window {

    void open();

    void setEventHandler(MultiPlayerMenuEventHandler eventHandler);

}
