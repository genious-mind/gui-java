package com.gitlab.genious_mind.gui_java.states.menu.singleplayer;

import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.util.MatchService;

import javax.inject.Inject;

public class SinglePlayerMenu implements State, SinglePlayerMenuEventHandler {

    private final SinglePlayerMenuWindow singleMenuWindow;
    private final MatchService matchService;
    private Context context;

    @Inject
    public SinglePlayerMenu(SinglePlayerMenuWindow singleMenuWindow, MatchService matchService) {
        this.singleMenuWindow = singleMenuWindow;
        this.matchService = matchService;
    }

    @Override
    public void start(Context context, Object... object) {
        this.context = context;

        singleMenuWindow.setEventHandler(this);
        singleMenuWindow.open(matchService.isThereAnOpenMatch());
    }

    @Override
    public void onContinue() {
        context.changeState(StateEnum.CONTINUE);
    }

    @Override
    public void onNewMatch() {
        context.changeState(StateEnum.CONFIGURATION, StateEnum.NEW);
    }

    @Override
    public void onRecord() {
        context.changeState(StateEnum.RECORD);
    }

    @Override
    public void onGoBack() {
        context.changeState(StateEnum.MAIN_MENU);
    }

    @Override
    public void stop() {
        singleMenuWindow.close();
    }

}
