package com.gitlab.genious_mind.gui_java.states.menu.singleplayer;

public interface SinglePlayerMenuEventHandler {

    void onContinue();

    void onNewMatch();

    void onGoBack();

    void onRecord();

}
