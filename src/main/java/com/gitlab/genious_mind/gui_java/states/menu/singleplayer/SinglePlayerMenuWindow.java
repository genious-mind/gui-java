package com.gitlab.genious_mind.gui_java.states.menu.singleplayer;

import com.gitlab.genious_mind.gui_java.common.Window;

public interface SinglePlayerMenuWindow extends Window {

    void open(boolean isContinueEnabled);

    void setEventHandler(SinglePlayerMenuEventHandler eventHandler);

}
