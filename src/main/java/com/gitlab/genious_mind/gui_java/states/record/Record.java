package com.gitlab.genious_mind.gui_java.states.record;

import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.common.state.StateEnum;
import com.gitlab.genious_mind.gui_java.util.MatchService;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;

import javax.inject.Inject;
import java.util.Collection;

public class Record implements State, RecordEventHandler {

    private final MatchService matchService;
    private final RecordWindow recordWindow;
    private Context context;

    @Inject
    public Record(RecordWindow recordWindow, MatchService matchService) {
        this.matchService = matchService;
        this.recordWindow = recordWindow;
        this.recordWindow.setEventHandler(this);
    }

    @Override
    public void start(Context context, Object... object) {
        this.context = context;

        Collection<Mastermind> matches = matchService.findAllWonMatches();
        recordWindow.open(matches);
    }

    @Override
    public void onGoBack() {
        context.changeState(StateEnum.SINGLE_PLAYER_MENU);
    }

    @Override
    public void stop() {
        recordWindow.close();
    }

}
