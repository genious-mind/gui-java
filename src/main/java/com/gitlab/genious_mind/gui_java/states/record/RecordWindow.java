package com.gitlab.genious_mind.gui_java.states.record;

import com.gitlab.genious_mind.gui_java.common.Window;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;

import java.util.Collection;
import java.util.Locale;

public interface RecordWindow extends Window {

    void open(Collection<Mastermind> matches);

    void setEventHandler(RecordEventHandler eventHandler);

}
