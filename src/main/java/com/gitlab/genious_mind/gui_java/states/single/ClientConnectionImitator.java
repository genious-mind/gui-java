package com.gitlab.genious_mind.gui_java.states.single;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.client.ClientReliableConnection;
import com.gitlab.genious_mind.gui_java.states.game.server.ServerGame;

import java.io.IOException;

public class ClientConnectionImitator implements ClientReliableConnection {

    private final ServerGame serverGame;

    public ClientConnectionImitator(ServerGame serverGame) {
        this.serverGame = serverGame;
    }

    @Override
    public void setMessageReceiver(ClientMessageReceiver messageReceiver) {

    }

    @Override
    public void start(Packet packet) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sendMessage(String message) {
        serverGame.onMessageReceived(0, message);
    }

    @Override
    public void close() {
    }

    @Override
    public Packet getLocalInfo() {
        throw new UnsupportedOperationException();
    }

}
