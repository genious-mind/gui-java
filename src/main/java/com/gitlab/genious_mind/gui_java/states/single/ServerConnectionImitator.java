package com.gitlab.genious_mind.gui_java.states.single;

import com.gitlab.genious_mind.gui_java.common.Packet;
import com.gitlab.genious_mind.gui_java.network.reliable.server.ServerMessageReceiver;
import com.gitlab.genious_mind.gui_java.network.reliable.server.ServerReliableConnection;
import com.gitlab.genious_mind.gui_java.states.game.client.ClientGame;

import java.io.IOException;

public class ServerConnectionImitator implements ServerReliableConnection {

    private final ClientGame clientGame;

    public ServerConnectionImitator(ClientGame clientGame) {
        this.clientGame = clientGame;
    }

    @Override
    public void start() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Packet getClientInfo(int clientIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getPort() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sendMessage(String message) {
        clientGame.onMessageReceived(message);
    }

    @Override
    public void sendMessage(String message, int clientIndex) {
        sendMessage(message);
    }

    @Override
    public void setMessageReceiver(ServerMessageReceiver serverMessageReceiver) {

    }

    @Override
    public void close() {

    }

}
