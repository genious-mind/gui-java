package com.gitlab.genious_mind.gui_java.states.single;

import com.gitlab.genious_mind.gui_java.StateManager;
import com.gitlab.genious_mind.gui_java.common.state.Context;
import com.gitlab.genious_mind.gui_java.common.state.State;
import com.gitlab.genious_mind.gui_java.states.game.client.ClientGame;
import com.gitlab.genious_mind.gui_java.states.game.client.GameWindow;
import com.gitlab.genious_mind.gui_java.states.game.server.ServerGame;
import com.gitlab.genious_mind.gui_java.util.MatchService;
import com.gitlab.genious_mind.gui_java.util.core.Mastermind;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;

@Log4j2
public class SinglePlayer implements State, SinglePlayerEventHandler {

    private final GameWindow gameWindow;
    private final MatchService matchService;
    private ServerGame serverGame;
    private ClientGame clientGame;

    @Inject
    public SinglePlayer(@Named("SinglePlayerGameWindow") GameWindow gameWindow, MatchService matchService) {
        this.gameWindow = gameWindow;
        this.matchService = matchService;
    }

    @Override
    public void onExit() {
        this.matchService.saveMatch(serverGame.getMastermind());
    }

    @Override
    public void onWin() {
        onExit();
    }

    @Override
    public void start(Context context, Object... args) {
        Mastermind mastermind = StateManager.getObjectFromArgs(args, 0, Mastermind.class);

        this.clientGame = new ClientGame(gameWindow, this, mastermind.getAttempts());
        this.serverGame = new ServerGame(new ServerConnectionImitator(clientGame), new HashMap<>(), 0, mastermind);

        serverGame.start();
        clientGame.start(context, new ClientConnectionImitator(serverGame), mastermind.getConfig());
    }

    @Override
    public void stop() {
        clientGame.stop();
        serverGame.close();
    }

}