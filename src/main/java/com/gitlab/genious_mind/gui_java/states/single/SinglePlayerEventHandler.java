package com.gitlab.genious_mind.gui_java.states.single;

public interface SinglePlayerEventHandler {

    void onExit();

    void onWin();

}
