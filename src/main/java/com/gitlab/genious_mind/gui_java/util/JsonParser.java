package com.gitlab.genious_mind.gui_java.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParser<T> {

    private final ObjectMapper mapper;
    private final TypeReference<T> type;

    public JsonParser(TypeReference<T> type) {
        this.type = type;
        this.mapper = new ObjectMapper();
    }

    public String toJson(T object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public T fromJson(String message) {
        try {
            return mapper.readValue(message, type);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
