package com.gitlab.genious_mind.gui_java.util;

import java.util.Collection;
import java.util.Locale;

public interface LanguageService {

    Collection<Locale> getAvailableLanguages();

    Locale getCurrentLanguage();

    void setCurrentLanguage(Locale currentLocale);

}
