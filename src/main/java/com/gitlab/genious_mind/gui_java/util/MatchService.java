package com.gitlab.genious_mind.gui_java.util;

import com.gitlab.genious_mind.gui_java.util.core.Mastermind;

import java.util.Collection;

public interface MatchService {

    Mastermind loadMastermind();

    void closeOpenMatch();

    void insertNewMatch(Mastermind mastermind);

    void saveMatch(Mastermind mastermind);

    boolean isThereAnOpenMatch();

    Collection<Mastermind> findAllWonMatches();

}
