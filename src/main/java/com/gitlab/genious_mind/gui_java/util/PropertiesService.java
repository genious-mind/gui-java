package com.gitlab.genious_mind.gui_java.util;

import java.util.Properties;

public interface PropertiesService {

    Properties loadDefaultProperties();

    Properties loadUserProperties();

    void saveUserProperties(Properties properties);

    void updateUserProperties(Properties properties);

}
