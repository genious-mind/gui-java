package com.gitlab.genious_mind.gui_java.util.core;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Getter
@Log4j2
public class Mastermind {

    private final List<MastermindAttempt> attempts;
    private final MastermindConfig config;
    private final int[] numberToGuess;
    private final long startTime;
    private long elapsedSeconds;
    private Long score;

    public Mastermind(MastermindConfig config) {
        this(config, generateNumber(config), System.currentTimeMillis(), 0, new ArrayList<>(), null);
    }

    public Mastermind(MastermindConfig config, int[] numberToGuess, long startTime, long elapsedSeconds, List<MastermindAttempt> attempts, Long score) {
        this.config = config;
        this.numberToGuess = numberToGuess;
        this.startTime = startTime;
        this.elapsedSeconds = elapsedSeconds;
        this.attempts = attempts;
        this.score = score;

        log.debug("Number to guess = {}", this.numberToGuess);
    }

    private static int getRandomDigit(int maxDigit) {
        return ThreadLocalRandom.current().nextInt(0, maxDigit);
    }

    private static int[] generateNumber(MastermindConfig config) {
        List<Integer> availableNumber = IntStream.range(config.getMinDigit(), config.getMaxDigit()).boxed().collect(Collectors.toList());
        int[] number = new int[config.getNumberLength()];

        for (int i = 0; i < config.getNumberLength(); i++) {
            int index = getRandomDigit(availableNumber.size());
            number[i] = availableNumber.remove(index);
        }

        return number;
    }

    public MastermindAttempt evaluateAttempt(int[] attemptedNumber) {
        if (!config.isNumberValidAndComplete(attemptedNumber)) {
            throw new NumberFormatException();
        }

        int digitsExact = exactPosition(attemptedNumber);
        int digitsPresent = samePosition(attemptedNumber) - digitsExact;

        MastermindAttempt attempt = new MastermindAttempt(attemptedNumber, digitsExact, digitsPresent, elapsedSeconds);
        this.attempts.add(attempt);

        if (config.getNumberLength() == digitsExact) {
            this.score = calculateScore(attempts.size(), elapsedSeconds);
        }

        return attempt;
    }

    private static long calculateScore(int attemptCount, long elapsedSeconds) {
        return attemptCount * elapsedSeconds;
    }

    private int samePosition(int[] number) {
        return (int) IntStream.of(number).filter(digit ->
                IntStream.of(numberToGuess).anyMatch(digitToGuess -> digitToGuess == digit)
        ).count();
    }

    private int exactPosition(int[] number) {
        int exactPosition = 0;
        for (int i = 0; i < numberToGuess.length; i++) {
            if (numberToGuess[i] == number[i]) {
                exactPosition++;
            }
        }
        return exactPosition;
    }

    public void updateTime() {
        elapsedSeconds++;
    }

    public boolean isGameOver() {
        return null != score;
    }

}
