package com.gitlab.genious_mind.gui_java.util.core;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class MastermindAttempt {

    private final int[] attempt;
    private final int digitsExact;
    private final int digitsPresent;
    private final long elapsedSeconds;

    @JsonCreator
    public MastermindAttempt(@JsonProperty("attempt") int[] attempt,
                      @JsonProperty("digitsExact") int digitsExact,
                      @JsonProperty("digitsPresent") int digitsPresent,
                      @JsonProperty("elapsedSeconds") long elapsedSeconds) {
        this.attempt = attempt;
        this.digitsExact = digitsExact;
        this.digitsPresent = digitsPresent;
        this.elapsedSeconds = elapsedSeconds;
    }

}



