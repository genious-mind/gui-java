package com.gitlab.genious_mind.gui_java.util.core;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.stream.IntStream;

@Getter
public class MastermindConfig {

    public static final int MIN_DIGIT = 3;
    public static final int MAX_DIGIT = 100;
    public static final int MIN_NUMBER_LENGTH = 2;
    public static final int MAX_NUMBER_LENGTH = 20;

    private final int minDigit;
    private final int maxDigit;
    private final int numberLength;

    @JsonCreator
    public MastermindConfig(@JsonProperty("maxDigit") int maxDigit,
                            @JsonProperty("numberLength") int numberLength) {
        if (!isValidConfiguration(maxDigit, numberLength)) {
            throw new NumberFormatException();
        }

        this.minDigit = 0;
        this.maxDigit = maxDigit;
        this.numberLength = numberLength;
    }

    private static boolean isValidConfiguration(int maxDigit, int numberLength) {
        return isValidNumberLength(numberLength)
                && isValidMaxDigit(maxDigit)
                && numberLength < maxDigit;
    }

    public static boolean isValidNumberLength(int numberLength) {
        return (MIN_NUMBER_LENGTH <= numberLength && numberLength <= MAX_NUMBER_LENGTH);
    }

    public static boolean isValidMaxDigit(int maxDigit) {
        return (MIN_DIGIT <= maxDigit && maxDigit <= MAX_DIGIT);
    }

    public boolean isNumberValid(int[] number) {
        return null != number
                && number.length <= numberLength
                && areDigitsUniques(number)
                && areAllDigitBetweenMinAndMaxValue(number);
    }

    public boolean isNumberValidAndComplete(int[] number) {
        return isNumberValid(number) && number.length == numberLength;
    }

    private static boolean areDigitsUniques(int[] number) {
        return getUniqueDigits(number).length == number.length;
    }

    private static Integer[] getUniqueDigits(int[] number) {
        return IntStream.of(number).boxed().distinct().toArray(Integer[]::new);
    }

    private boolean areAllDigitBetweenMinAndMaxValue(int[] number) {
        return IntStream.of(number).allMatch(digit -> minDigit <= digit && digit < maxDigit);
    }

}
