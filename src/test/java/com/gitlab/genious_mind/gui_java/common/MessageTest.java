package com.gitlab.genious_mind.gui_java.common;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gitlab.genious_mind.gui_java.common.message.Message;
import com.gitlab.genious_mind.gui_java.common.message.MessageType;
import com.gitlab.genious_mind.gui_java.util.JsonParser;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


class MessageTest {

    @Test
    void toJson_whenMessageNull_returnValidJson() {
        Message<Object> message = new Message<>(MessageType.UPDATE_SERVER_LIST);
        assertThat(message.toJson()).isEqualTo("{\"type\":\"UPDATE_SERVER_LIST\",\"message\":null}");
    }

    @Test
    void toJson_whenTypeNull_throwNullPointerException() {
        assertThatThrownBy(() -> new Message<>(null)).isInstanceOf(NullPointerException.class);
    }

    @Test
    void toJson_whenMessageIsList_returnValidJson() {
        Message<Object> message = new Message<>(MessageType.UPDATE_SERVER_LIST, Arrays.asList(1, 2, 3, 4));
        assertThat(message.toJson()).isEqualTo("{\"type\":\"UPDATE_SERVER_LIST\",\"message\":[1,2,3,4]}");
    }

    @Test
    void fromJson_whenMessageIsIntegerList_returnValidMessage() {
        String json = new JsonParser<>(new TypeReference<>() {
        }).toJson(new Message<>(MessageType.UPDATE_SERVER_LIST, Arrays.asList(1, 2, 3, 4)));

        Collection<Integer> message = Message.getMessageFromJson(new TypeReference<>() {
        }, json);

        assertThat(message).isEqualTo(Arrays.asList(1, 2, 3, 4));
    }

    @Test
    void fromJson_whenMessageIsObjectList_returnValidMessage() {
        Packet packet = new Packet("1.2.3.4", 100);
        String json = new JsonParser<>(new TypeReference<>() {
        }).toJson(new Message<>(MessageType.UPDATE_SERVER_LIST, Collections.singleton(packet)));

        Collection<Packet> message = Message.getMessageFromJson(new TypeReference<>() {
        }, json);

        assertThat(message).containsExactlyInAnyOrder(packet);
    }

}
