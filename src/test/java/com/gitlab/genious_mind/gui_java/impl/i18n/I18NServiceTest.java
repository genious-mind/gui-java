package com.gitlab.genious_mind.gui_java.impl.i18n;

import com.gitlab.genious_mind.gui_java.impl.filesystem.PropertiesLoader;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class I18NServiceTest {

    private static final Locale DEFAULT_LOCALE = Locale.JAPANESE;
    private static final String DEFAULT_STRING = "es";

    @Test
    void newI18nService_whenFilenameUppercase_returnLocale() throws IOException, URISyntaxException {
        PropertiesLoader propertiesLoader = getPropertiesLoaderMock("LOCALE_EN.PROPERTIES");
        I18nService service = new I18nService(propertiesLoader, DEFAULT_LOCALE, DEFAULT_STRING);
        assertThat(service.getAvailableLanguages()).contains(Locale.ENGLISH);
    }

    @Test
    void newI18nService_whenFilenameLowercase_returnLocale() throws IOException, URISyntaxException {
        PropertiesLoader propertiesLoader = getPropertiesLoaderMock("locale_it.properties");
        I18nService service = new I18nService(propertiesLoader, DEFAULT_LOCALE, DEFAULT_STRING);
        assertThat(service.getAvailableLanguages()).contains(Locale.ITALIAN);
    }

    @Test
    void newI18nService_whenFilenameMixedcase_returnLocale() throws IOException, URISyntaxException {
        PropertiesLoader propertiesLoader = getPropertiesLoaderMock("LoCaLe_eN.PRoPeRTieS");
        I18nService service = new I18nService(propertiesLoader, DEFAULT_LOCALE, DEFAULT_STRING);
        assertThat(service.getAvailableLanguages()).contains(Locale.ENGLISH);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "locale_.properties",
            "locale_s.properties",
            "locale_eNg.properties",
            "locale_18.properties",
            "locale_g8.properties",
            "local_en.properties",
            "locale_en.property",
            "locale_UU.properties",
    })
    void newI18nService_whenFilenameIsInvalid_returnEmpty(String filename) throws IOException, URISyntaxException {
        PropertiesLoader propertiesLoader = getPropertiesLoaderMock(filename);
        I18nService service = new I18nService(propertiesLoader, DEFAULT_LOCALE, DEFAULT_STRING);
        assertThat(service.getAvailableLanguages()).isEmpty();
    }

    @Test
    void newI18nService_whenFilenameIsComplete_returnLocale() throws IOException, URISyntaxException {
        String path = "/home/user/project/target/classes/locale/locale_en.properties";
        PropertiesLoader propertiesLoader = getPropertiesLoaderMock(path);
        I18nService service = new I18nService(propertiesLoader, DEFAULT_LOCALE, DEFAULT_STRING);
        assertThat(service.getAvailableLanguages()).contains(Locale.ENGLISH);
    }

    @Test
    void newI18nService_whenLanguageInFilenameIsNotStandard_returnLocale() throws IOException, URISyntaxException {
        PropertiesLoader propertiesLoader = getPropertiesLoaderMock("locale_ru.properties");
        I18nService service = new I18nService(propertiesLoader, DEFAULT_LOCALE, DEFAULT_STRING);
        assertThat(service.getAvailableLanguages()).contains(new Locale("RU"));
    }

    @Test
    void newI18nService_whenLanguageInFilenamesAreDuplicated_returnShortestPath() throws IOException, URISyntaxException {
        PropertiesLoader propertiesLoader = getPropertiesLoaderMock("/resources/locale_en.properties", "/resources/locale/locale_en.properties");

        I18nService service = new I18nService(propertiesLoader, DEFAULT_LOCALE, DEFAULT_STRING);
        assertThat(service.getAvailableLanguages()).contains(Locale.ENGLISH);
        assertThat(service.getString("property1")).isEqualTo("file1");
    }

    @Test
    void newI18nService_whenLanguageInFilenamesAreDuplicated_returnLowercase() throws IOException, URISyntaxException {
        PropertiesLoader propertiesLoader = getPropertiesLoaderMock("LOCALE_EN.PROPERTIES", "locale_en.properties");

        I18nService service = new I18nService(propertiesLoader, DEFAULT_LOCALE, DEFAULT_STRING);
        assertThat(service.getAvailableLanguages()).contains(Locale.ENGLISH);
        assertThat(service.getString("property1")).isEqualTo("file2");
    }

    private PropertiesLoader getPropertiesLoaderMock(String path1, String path2) throws URISyntaxException, IOException {
        PropertiesLoader propertiesLoader = Mockito.mock(PropertiesLoader.class);
        Path uppercasePath = Paths.get(path1);
        Path lowercasePath = Paths.get(path2);

        when(propertiesLoader.findAllFilesInClasspathFolder(any(String.class)))
                .thenReturn(Stream.of(uppercasePath, lowercasePath));
        when(propertiesLoader.getPropertiesFromFile(eq(uppercasePath)))
                .thenReturn(new Properties() {{
                    put("property1", "file1");
                    put("property2", "file1");
                }});
        when(propertiesLoader.getPropertiesFromFile(eq(lowercasePath)))
                .thenReturn(new Properties() {{
                    put("property1", "file2");
                }});

        return propertiesLoader;
    }

    private static PropertiesLoader getPropertiesLoaderMock(String filename) throws URISyntaxException, IOException {
        PropertiesLoader propertiesLoader = Mockito.mock(PropertiesLoader.class);
        when(propertiesLoader.findAllFilesInClasspathFolder(any(String.class))).thenReturn(Stream.of(Paths.get(filename)));
        when(propertiesLoader.getPropertiesFromFile(any(Path.class))).thenReturn(new Properties());
        return propertiesLoader;
    }

}
